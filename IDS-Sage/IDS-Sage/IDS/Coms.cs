﻿#nullable enable

using AzureRemoteService;
using IDS_Sage.IDS.Http;
using Protocol.Data;

namespace IDS_Sage.IDS;

internal class Communications
{
//#if DEBUG
	//const string Link = "http://jbtest.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";
//#else
	private const string PRIMARY_LINK = "http://users.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";

	private const string SECONDARY_LINK = "http://reports.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";
//#endif

	public enum SERVER
	{
		PRIMARY,
		SECONDARY,
		CORE
	}

	internal Communications( SERVER server, string carrierId, string accountId, string userId, string password, int lastInvoiceNumber, string provider )
	{
		StartingInvoice = lastInvoiceNumber.ToString();
		CarrierId       = carrierId;
		UserName        = userId;
		Password        = password;

		if( server != SERVER.CORE )
		{
			var Link = server == SERVER.PRIMARY ? PRIMARY_LINK : SECONDARY_LINK;

			ComsLink = Link.Replace( "%CID%", carrierId )
						   .Replace( "%ACID%", accountId )
						   .Replace( "%UID%", userId )
						   .Replace( "%PASS%", password )
						   .Replace( "%INV_ID%", StartingInvoice )
						   .Replace( "%REPORT%", provider );
		}
	}

	private readonly string? ComsLink; // Null if CORE

	private readonly string StartingInvoice,
							CarrierId,
							UserName,
							Password;

	internal async Task GetCsv( Action<Stream> onSuccess, Action<string> onFail )
	{
		string Csv;

		if( ComsLink is null ) // CORE
		{
			var (Status, Client) = await Azure.LogIn( "Sage 300", CarrierId, UserName, Password );

			if( Status == Azure.AZURE_CONNECTION_STATUS.OK )
			{
				Csv = await Client.RequestExportCsv( new ExportArgs
													 {
														 ExportName = "Sage300",
														 StringArg1 = StartingInvoice // Starting Invoice Number
													 } );
			}
			else
			{
				onFail( "Cannot connect to Core" );
				return;
			}
		}
		else
		{
			Csv = "";
			var ReplyError = false;
			var Reason     = "";

			HttpIo.WebIo.Get( new HttpIo.RequestEntry( ComsLink,
													   reply =>
													   {
														   Csv = reply.Content;
													   },
													   reply =>
													   {
														   ReplyError = true;
														   Reason     = reply.Content.Trim();
													   },
													   true
													 )
							);

			if( ReplyError || Csv.Contains( Reason = "Authentication Error" ) )
			{
				onFail( Reason );
				return;
			}
		}
		onSuccess( new MemoryStream( Encoding.UTF8.GetBytes( Csv ) ) );
	}
}