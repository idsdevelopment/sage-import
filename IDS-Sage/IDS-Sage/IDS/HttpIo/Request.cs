

// ReSharper disable InconsistentNaming

namespace IDS_Sage.IDS.Http;

public partial class HttpIo
{
	public class ParameterEntry
	{
		public bool IsBinary { get; }


		public bool IsFile => IsBinary && !string.IsNullOrEmpty( FileName );

		public string AsString => IsBinary ? Data == null ? "" : Encoding.ASCII.GetString( Data ) : StringData ?? "";

		public byte[] AsBytes => IsBinary ? Data ?? Array.Empty<byte>() : Encoding.ASCII.GetBytes( StringData ?? "" );

		public           string FileName { get; }
		private readonly byte[] Data;
		private readonly string StringData;

		public ParameterEntry( byte[] data )
		{
			IsBinary = true;
			Data     = data;
		}

		public ParameterEntry( string stringOrFileName, bool isFile = false )
		{
			IsBinary = isFile;

			if( isFile )
			{
				using var Stream = new FileStream( FileName = stringOrFileName, FileMode.Open, FileAccess.Read );

				using var Reader = new BinaryReader( Stream );

				Data = Reader.ReadBytes( (int)Stream.Length );
			}
			else
				StringData = stringOrFileName;
		}

		public ParameterEntry( string fileName, MemoryStream Stream )
		{
			IsBinary   = true;
			Data       = Stream.ToArray();
			StringData = fileName;
		}


		public ParameterEntry( string fileName, byte[] data )
		{
			IsBinary = true;
			FileName = fileName;
			Data     = data;
		}
	}

	public class Parameters : Dictionary<string, ParameterEntry>
	{
		public static string ValueAsString( KeyValuePair<string, ParameterEntry> Kvp ) => Kvp.Value.AsString;

		public static string KeyAsString( KeyValuePair<string, ParameterEntry> Kvp ) => Kvp.Key;

		public static string ValueAsHtmlString( KeyValuePair<string, ParameterEntry> Kvp ) => WebUtility.HtmlEncode( Kvp.Value.AsString );

		public static string KeyAsHtmlString( KeyValuePair<string, ParameterEntry> Kvp ) => WebUtility.HtmlEncode( Kvp.Key );

		public void Add( string key, byte[] data, bool isFile = false )
		{
			base.Add( key, isFile ? new ParameterEntry( key, data ) : new ParameterEntry( data ) );
		}

		public void Add( string key, string stringOrFileName, bool isFile = false )
		{
			base.Add( key, new ParameterEntry( stringOrFileName, isFile ) );
		}

		public void Add( string key, bool isFile = false )
		{
			base.Add( key, new ParameterEntry( key, isFile ) );
		}

		public void Add( string key, MemoryStream stream )
		{
			base.Add( key, new ParameterEntry( key, stream ) );
		}


		public override string ToString()
		{
			var RetVal = "";

			foreach( var Bytes in this )
			{
				if( RetVal != "" )
					RetVal += "&";

				RetVal += KeyAsHtmlString( Bytes ) + "=" + ValueAsHtmlString( Bytes );
			}

			return RetVal;
		}
	}

	public class RequestEntry : Parameters
	{
		public enum REQUEST_TYPE
		{
			POST,
			GET
		}

		internal class Semaphore
		{
			internal bool Waiting
			{
				get
				{
					lock( LockObject )
						return _Waiting;
				}

				set
				{
					lock( LockObject )
						_Waiting = value;
				}
			}

			private          bool   _Waiting;
			private readonly object LockObject = new();

			public static implicit operator Semaphore( bool value ) => new() { Waiting = value };


			public static implicit operator bool( Semaphore value ) => value.Waiting;
		}


		public bool Waiting
		{
			get => _Waiting;

			internal set => _Waiting.Waiting = value;
		}

		internal Semaphore _Waiting = new();
		public   byte[]    BinaryContent;
		public   string    Content;

		public CookieContainer Cookies;
		public string          Error;

		public bool IsMultiPart;

		public Action<RequestEntry> OnSuccess,
		                            OnFail;

		public REQUEST_TYPE Operation;
		public int          Timeout;

		public string Url;

		public RequestEntry( string url, Action<RequestEntry> onSuccess, Action<RequestEntry> onFail, bool wait = false, int timeOut = INITIAL_TIMEOUT )
		{
			Url       = url;
			OnSuccess = onSuccess;
			OnFail    = onFail;
			Timeout   = timeOut;
			Waiting   = wait;

			lock( CookieLock )
			{
				if( GlobalCookies == null )
					InitCookie();

				Cookies = GlobalCookies;
			}
		}
	}


	public class RequestQueue : Queue<RequestEntry>
	{
		public new int Count
		{
			get
			{
				lock( LockObject )
					return base.Count;
			}
		}

		public RequestEntry NextEntry
		{
			get
			{
				lock( LockObject )
					return Dequeue();
			}
		}

		private readonly object LockObject = new();

		public void Add( RequestEntry entry )
		{
			lock( LockObject )
				Enqueue( entry );
		}
	}
}