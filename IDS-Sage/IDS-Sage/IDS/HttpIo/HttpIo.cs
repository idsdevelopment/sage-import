namespace IDS_Sage.IDS.Http;

public partial class HttpIo : Disposable
{
	public static HttpIo WebIo => _WebIo ??= new HttpIo(); // Had Problem with static initialisation

	private static HttpIo _WebIo;

	protected override void OnDispose( bool systemDisposing )
	{
		Closing = true;

		while( Closing )
			Thread.Sleep( 100 );

		lock( CookieLock )
			GlobalCookies = null;
	}

	public HttpIo()
	{
		InitCookie();

		Task.Factory.StartNew( () =>
							   {
								   while( !Closing )
								   {
									   if( WebIoQueue.Count > 0 )
									   {
										   var R = WebIoQueue.NextEntry;

										   try
										   {
											   var Request = (HttpWebRequest)WebRequest.Create( R.Url );

											   var Sp = Request.ServicePoint;
											   Sp.SetTcpKeepAlive( true, 60 * 1000, 60 * 1000 );

											   var T = R.Timeout;
											   Request.Timeout          = T;
											   Request.ReadWriteTimeout = T;
											   Request.ContinueTimeout  = T;

											   Request.CachePolicy     = NoCachePolicy;
											   Request.CookieContainer = R.Cookies;

											   if( R.Operation == RequestEntry.REQUEST_TYPE.POST )
											   {
												   Request.Method = "POST";

												   var Boundary = "--------B0UnDarY---------" + DateTime.Now.Ticks.ToString( "x" );

												   Request.ContentType = "multipart/form-data; boundary=" + Boundary;
												   Request.Credentials = CredentialCache.DefaultCredentials;

												   using Stream MemStream = new MemoryStream();

												   try
												   {
													   var BoundaryBytes = Encoding.ASCII.GetBytes( "\r\n--" + Boundary + "\r\n" );

													   var FormdataTemplate = "\r\n--" + Boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

													   foreach( var Kv in R )
													   {
														   if( !Kv.Value.IsFile )
														   {
															   var FormItem      = string.Format( FormdataTemplate, Parameters.KeyAsString( Kv ), Parameters.ValueAsString( Kv ) );
															   var FormItemBytes = Encoding.UTF8.GetBytes( FormItem );
															   MemStream.Write( FormItemBytes, 0, FormItemBytes.Length );
														   }
													   }

													   MemStream.Write( BoundaryBytes, 0, BoundaryBytes.Length );

													   const string FILE_TEMPLATE = "Content-Disposition: form-data;name=\"{0}\";filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n";
													   var          I             = 0;

													   foreach( var Kv in R )
													   {
														   if( Kv.Value.IsFile )
														   {
															   var Header      = string.Format( FILE_TEMPLATE, "File" + I++, Kv.Value.FileName );
															   var HeaderBytes = Encoding.UTF8.GetBytes( Header );
															   MemStream.Write( HeaderBytes, 0, HeaderBytes.Length );

															   var Data = Kv.Value.AsBytes;
															   MemStream.Write( Data, 0, Data.Length );

															   MemStream.Write( BoundaryBytes, 0, BoundaryBytes.Length );
														   }
													   }

													   var RequestStream = Request.GetRequestStream();

													   try
													   {
														   var TempBuffer = new byte[ MemStream.Length ];
														   MemStream.Position = 0;

														   var Offset = 0;

														   while( true )
														   {
															   var BytesRead = MemStream.Read( TempBuffer, Offset, TempBuffer.Length );

															   if( BytesRead == 0 )
																   break;

															   Offset += BytesRead;
														   }
														   MemStream.Close();

														   RequestStream.Write( TempBuffer, 0, TempBuffer.Length );
													   }
													   finally
													   {
														   RequestStream.Close();
													   }
												   }
												   finally
												   {
													   MemStream.Close();
												   }
											   }
											   else // Url has GET values
												   Request.Method = "GET";

											   Request.Timeout = R.Timeout;

											   string Content       = null;
											   byte[] BinaryContent = null;

											   using( var Response = (HttpWebResponse)Request.GetResponse() )
											   {
												   if( Response.StatusCode == HttpStatusCode.OK )
												   {
													   try
													   {
														   using var ResponseStream = Response.GetResponseStream();

														   using var Stream = new MemoryStream();

														   int Count;
														   var Buffer = new byte[ 4096 ];

														   do
														   {
															   Count = ResponseStream.Read( Buffer, 0, Buffer.Length );
															   Stream.Write( Buffer, 0, Count );
														   }
														   while( Count != 0 );

														   BinaryContent = Stream.ToArray();
														   Content       = Encoding.ASCII.GetString( BinaryContent );
													   }
													   finally
													   {
														   Response.Close();
													   }
												   }

												   lock( CookieLock )
												   {
													   if( GlobalCookies == null )
														   InitCookie();

													   try
													   {
														   GlobalCookies.Add( Response.Cookies );
													   }
													   catch
													   {
													   }

													   R.Cookies = GlobalCookies;
												   }
											   }

											   if( BinaryContent is not null )
											   {
												   R.Error         = "Ok";
												   R.Content       = Content;
												   R.BinaryContent = BinaryContent;

												   R?.OnSuccess( R );
											   }
											   else if( R.OnFail is not null )
											   {
												   R.Content = R.Error = "No Response";
												   R.OnFail( R );
											   }
										   }
										   catch( Exception E )
										   {
										   #if DEBUG
											   Debug.Print( $"Http Response Exception: {E.Message}" );
										   #endif
											   R.Content = R.Error = E.Message;
											   R.OnFail( R );
										   }
										   finally
										   {
											   R.Waiting = false;
										   }
									   }
									   else
										   Thread.Sleep( 100 );
								   }
								   Closing = false;
							   } );
	}

	public void Post( RequestEntry entry )
	{
		entry.Operation = RequestEntry.REQUEST_TYPE.POST;
		WebIoQueue.Add( entry );

		while( entry.Waiting )
			Thread.Sleep( 100 );
	}

	public void Get( RequestEntry entry )
	{
		entry.Operation = RequestEntry.REQUEST_TYPE.GET;
		WebIoQueue.Add( entry );

		while( entry.Waiting )
			Thread.Sleep( 100 );
	}
}