﻿namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo : Disposable
	{
		public static void InitCookie()
		{
			lock( CookieLock )
			{
				GlobalCookies = new CookieContainer();
			}
		}
	}
}