﻿using IDS_Sage.DataSerialised;
using System.Runtime.Serialization;

namespace IDS_Sage.Database
{
	[Serializable()]
	public class DbCustomers : SerialisedData
	{

		public DbCustomers()
		{
		}

		public DbCustomers( SerializationInfo info, StreamingContext context )
			: base( info, context )
		{
		}


		public static string PackCode( string code )
		{
			var RetVal = new StringBuilder();

			code = code.Trim().ToUpper();
//			bool Skip = false;

			foreach( var C in code )
			{
				if( ( C >= 'A' && C <= 'Z' ) || ( C >= '0' && C <= '9' ) )
				{
					RetVal.Append( C );
//					Skip = false;
				}
/*				else if( !Skip )
				{
					RetVal.Append( ' ' );
					Skip = true;
				}
*/			}
			return RetVal.ToString().Trim();
		}


		private static bool CompareCustomerCode( string c1, string c2 )
		{
			c1 = PackCode( c1 );
			c2 = PackCode( c2 );
			return( c1 == c2 );
		}


		public bool HasChanged( bool ignoreAddress, Customer c, string customerName,
								   string suite, string addressLine1, string addressLine2,
								   string city, string region, string postCode,
								   string country, string contactName, string emailAddress,
								   string telephone, string notes )
		{
			if( c == null || !CompareCustomerCode( c.CustomerName, customerName ) )
				return true;

			if( ignoreAddress )
				return false;

			return ( c.Suite != suite.Trim()
					|| c.AddressLine1 != addressLine1.Trim() || c.AddressLine2 != addressLine2.Trim()
 					|| c.City != city.Trim() || c.Region != region.Trim() || c.PostCode != postCode.Trim()
					|| c.Country != country.Trim()
					|| c.ContactName != contactName.Trim() || c.EmailAddress != emailAddress.Trim()
					|| c.Telephone != telephone.Trim() || c.Notes != notes.Trim() );
		}



		public bool HasChanged( bool ignoreAddress, string idsAccount, string customerName,
									   string suite, string addressLine1, string addressLine2,
									   string city, string region, string postCode,
									   string country, string contactName, string emailAddress,
									   string telephone, string notes, out string oldCustomerName )
		{
			var C = GetCustomerByAccountId( idsAccount );
			if( C != null )
			{
				oldCustomerName = C.CustomerName;

				return HasChanged( ignoreAddress, C, customerName,
								   suite, addressLine1, addressLine2,
								   city, region, postCode,
								   country, contactName, emailAddress,
								   telephone, notes );
			}
			oldCustomerName = customerName;
			return true;
		}


		public bool HasChanged( bool ignoreAddress, Customer cust, out string oldCustomerName )
		{
			return HasChanged( ignoreAddress, cust.IdsAccount, cust.CustomerName, cust.Suite, cust.AddressLine1, cust.AddressLine2, cust.City, cust.Region, cust.PostCode, cust.Country,
							   cust.ContactName, cust.EmailAddress, cust.Telephone, cust.Notes, out oldCustomerName );
		}


		public bool Update( bool ignoreAddress, Customer cust, out string oldCustKey )
		{
			var Retval = HasChanged( ignoreAddress, cust, out oldCustKey );
			if( Retval )
				Add( cust );
			return Retval;
		}
	}
}
