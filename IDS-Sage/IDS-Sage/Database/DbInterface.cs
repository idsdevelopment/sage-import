﻿namespace IDS_Sage.Database
{
	internal interface IDbInterface
	{
		void Open( string dbPath );
		void Close();
		Customer GetCustomerByAccountId( string accountId );
		void Add( Customer custRec );
		void Remove( string accountId );
		void EmptyTable();
	}
}
