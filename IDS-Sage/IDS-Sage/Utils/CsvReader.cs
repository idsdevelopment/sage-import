﻿using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;

namespace IDS_Sage
{
	internal class CsvReader : List<CsvReader.Columns>
	{
		internal class Columns : List<string>
		{
		}

		internal void WriteToStream( Stream stream )
		{
			using var SWriter = new StreamWriter( stream );

			using var CWriter = new CsvWriter( SWriter, new CsvConfiguration( CultureInfo.InvariantCulture )
			                                            {
				                                            HasHeaderRecord = false
			                                            } );

			foreach( var Row in this )
			{
				foreach( var Col in Row )
					CWriter.WriteField( Col );

				CWriter.NextRecord();
			}
		}

		internal CsvReader( Stream fileStream )
		{
			var TempStream = new MemoryStream();
			fileStream.Position = 0;
			fileStream.CopyTo( TempStream );

			TempStream.Position = 0;

			using var SReader = new StreamReader( TempStream );

			using var CReader = new CsvHelper.CsvReader( SReader, new CsvConfiguration( CultureInfo.InvariantCulture )
			                                                      {
				                                                      HasHeaderRecord = false
			                                                      } );

			while( CReader.Read() )
			{
				var NewRow = new Columns();

				var I = 0;

				while( CReader.TryGetField<string>( I++, out var Field ) )
					NewRow.Add( Field );

				Add( NewRow );
			}
		}

		internal CsvReader( string fileName )
			: this( new FileStream( fileName, FileMode.Open, FileAccess.Read ) )
		{
		}
	}
}