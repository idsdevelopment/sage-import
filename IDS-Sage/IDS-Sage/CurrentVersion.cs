﻿namespace IDS_Sage;

internal class CurrentVersion
{
	public const string VERSION = " 2024.2.0";
}