﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace IDS_Sage.DataSerialised
{
	[Serializable()]
	public class SerialisedData : Dictionary<string, Customer>, IDbInterface
	{
		[NonSerializedAttribute]
		private string DbFile;

		[NonSerializedAttribute]
		private const string DB_NAME = "Customers.dict";

		public SerialisedData()
		{
		}

		public SerialisedData( SerializationInfo info, StreamingContext context )
			: base( info, context )
		{
		}

		public void Open( string dbPath )
		{
			var Path = Utils.AddPathSeparator( dbPath );

			if( !Directory.Exists( Path ) )
				Directory.CreateDirectory( Path );

			DbFile = Path + DB_NAME;
			Clear();

			try
			{
				using var CustFile = new FileStream( DbFile, FileMode.Open, FileAccess.Read, FileShare.None );

				IFormatter Formatter = new BinaryFormatter();
				CustFile.Position = 0;

				var Temp = (SerialisedData)Formatter.Deserialize( CustFile );

				foreach( var Kvp in Temp )
					Add( Kvp.Key, Kvp.Value );
			}
			catch
			{
			}
		}

		public void Close()
		{
			using var CustFile = new FileStream( DbFile, FileMode.Create, FileAccess.Write, FileShare.None );

			var Formatter = new BinaryFormatter();
			Formatter.Serialize( CustFile, this );

			CustFile.Flush();
			CustFile.Close();
		}

		public Customer GetCustomerByAccountId( string accountId )
		{
			Customer Retval;
			if( !TryGetValue( accountId.Trim(), out Retval ) )
				Retval = null;
			return Retval;
		}

		public void Add( Customer custRec )
		{
			var accountId = custRec.IdsAccount.Trim();
			if( GetCustomerByAccountId( accountId ) == null )
				Add( accountId, custRec );
			else
				this[ accountId ] = custRec;
		}

		new public void Remove( string accountId )
		{
			base.Remove( accountId.Trim() );

		}

		public void EmptyTable()
		{
			Clear();
		}


	}
}
