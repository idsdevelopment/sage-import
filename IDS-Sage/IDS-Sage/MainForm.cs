﻿using System.Linq;
using Utils.Csv;

namespace IDS_Sage;

public partial class MainForm : Form
{
	private const string SAI        = ".sai",
						 ERROR_PATH = @"Error\";

	public MainForm()
	{
		InitializeComponent();
	}

	private bool AllowClose;

	private void LoadSettings()
	{
		var D = Settings.Default;
		D.Reload();

		SalesLedger.Text         = D.SalesLedger;
		FuelSurchargeLedger.Text = D.FuelSurcharge;
		UserName.Text            = D.UserName;
		Password.Text            = D.Password;
		SageDatabase.Text        = D.SageDatabase;

		CarrierId.Text          = D.CarrierId;
		AccountId.Text          = D.AccountId;
		Provider.Text           = D.Provider;
		UserId.Text             = D.UserId;
		IdsPassword.Text        = D.IdsPassword;
		LastInvoiceNumber.Value = D.IDS_InvoiceNumber;
		ManualImportPath.Text   = D.ManualImportPath;
		ErrorPath.Text          = D.ErrorPath;
		DatabasePath.Text       = D.DatabasePath;
		DebugMode.Checked       = D.DebugMode;
		NextPollTime.Value      = D.NextPolTime;
		NextPollInterval.Value  = D.NextImportInterval;

		DontUpdateAddresses.Checked = D.DontUpdateAddresses;

		PrimaryServer.Checked   = D.PrimaryServer;
		SecondaryServer.Checked = D.SecondaryServer;
		CoreServer.Checked      = D.CoreServer;
	}

	private void SaveSettings()
	{
		var D = Settings.Default;

		D.SalesLedger   = SalesLedger.Text;
		D.FuelSurcharge = FuelSurchargeLedger.Text;
		D.UserName      = UserName.Text;
		D.Password      = Password.Text;
		D.SageDatabase  = SageDatabase.Text;

		D.CarrierId         = CarrierId.Text;
		D.AccountId         = AccountId.Text;
		D.Provider          = Provider.Text;
		D.UserId            = UserId.Text;
		D.IdsPassword       = IdsPassword.Text;
		D.IDS_InvoiceNumber = LastInvoiceNumber.Value;
		D.ManualImportPath  = ManualImportPath.Text;
		D.ErrorPath         = ErrorPath.Text;
		D.DatabasePath      = DatabasePath.Text;
		D.DebugMode         = DebugMode.Checked;

		D.NextPolTime        = NextPollTime.Value;
		D.NextImportInterval = NextPollInterval.Value;

		D.DontUpdateAddresses = DontUpdateAddresses.Checked;

		D.PrimaryServer   = PrimaryServer.Checked;
		D.SecondaryServer = SecondaryServer.Checked;
		D.CoreServer      = CoreServer.Checked;

		D.Save();
	}

	private void MainForm_Load( object sender, EventArgs e )
	{
		LoadSettings();

		CloseSettingsToolStripMenuItem_Click( sender, e );
	}

	private void ExitToolStripMenuItem_Click( object sender, EventArgs e )
	{
		AllowClose = true;
		Close();
	}

	private void Button1_Click( object sender, EventArgs e )
	{
		var SName = SageDatabase.Text;

		try
		{
			OpenFileDialog.FileName         = Path.GetFileName( SName );
			OpenFileDialog.InitialDirectory = Path.GetDirectoryName( SName );
		}
		catch( ArgumentException )
		{
			OpenFileDialog.FileName         = "";
			OpenFileDialog.InitialDirectory = "";
		}

		if( OpenFileDialog.ShowDialog() == DialogResult.OK )
			SageDatabase.Text = OpenFileDialog.FileName;
	}

	private void CloseSettingsToolStripMenuItem_Click( object sender, EventArgs e )
	{
		CloseSettings.Enabled = false;
		var Pages = MainTabControl.TabPages;
		Pages.Remove( ImportSettingsTabPage );
		Pages.Remove( SageSettingsTabPage );
		Pages.Remove( StatusTabPage );
		Pages.Add( StatusTabPage );

		SaveSettings();
	}

	private void ImportSettingsMenuItem_Click( object sender, EventArgs e )
	{
		CloseSettings.Enabled = true;
		var Pages = MainTabControl.TabPages;
		Pages.Remove( StatusTabPage );
		Pages.Remove( SageSettingsTabPage );
		Pages.Add( ImportSettingsTabPage );
	}

	private void SageToolStripMenuItem_Click( object sender, EventArgs e )
	{
		CloseSettings.Enabled = true;
		var Pages = MainTabControl.TabPages;
		Pages.Remove( StatusTabPage );
		Pages.Remove( ImportSettingsTabPage );
		Pages.Add( SageSettingsTabPage );
	}

	private void SageDatabase_Leave( object sender, EventArgs e )
	{
		try
		{
			var Txt = SageDatabase.Text;
			SageDatabase.Text = Path.GetDirectoryName( Txt ) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension( Txt ) + SAI;
		}
		catch
		{
		}
	}

	private void TestConnectionBtn_Click( object sender, EventArgs e )
	{
		var SName     = SageDatabase.Text;
		var Extension = Path.GetExtension( SName );

		if( ( Extension != null ) && ( Extension.ToLower() != SAI ) )
			MessageBox.Show( @"Invalid Sage File Name.", @"Error", MessageBoxButtons.OK );
		else
		{
			try
			{
				if( SDKInstanceManager.Instance.OpenDatabase( SName, UserName.Text.Trim(), Password.Text, true, "Ids-Sage", "IDSSA", 1 ) )
				{
					try
					{
						MessageBox.Show( @"Connected", @"Connected", MessageBoxButtons.OK );
						SDKInstanceManager.Instance.CloseDatabase();
						return;
					}
					catch( Exception E )
					{
						// ReSharper disable once LocalizableElement
						MessageBox.Show( "Sage Error:\r\n" + E.Message, @"Error", MessageBoxButtons.OK );
					}
				}
				MessageBox.Show( @"Cannot Open Sage Database", @"Error", MessageBoxButtons.OK );
			}
			catch( Exception E )
			{
				// ReSharper disable once LocalizableElement
				MessageBox.Show( "Sage Error:\r\n" + E.Message, @"Error", MessageBoxButtons.OK );
			}
		}
	}

	private void ManualImportPath_Leave( object sender, EventArgs e )
	{
		var Path = Utils.AddPathSeparator( ManualImportPath.Text );
		ManualImportPath.Text = Path;
		ErrorPath.Text        = Path + ERROR_PATH;
	}

	// Needed because of Bug in radio buttons
	private void MainForm_Shown( object sender, EventArgs e )
	{
		// ReSharper disable once JoinDeclarationAndInitializer
		// ReSharper disable once ConvertToConstant.Local
		var Version = CurrentVersion.VERSION;
	#if DEBUG
		Version += " (TEST VERSION)";
	#endif
		Text += Version;
	}

	private void MainForm_FormClosed( object sender, FormClosedEventArgs e )
	{
		SaveSettings();
	}

	private bool ShowBalloon( string txt )
	{
		var Retval = TrayIcon.Visible;

		if( Retval )
		{
			TrayIcon.BalloonTipText = txt;
			TrayIcon.ShowBalloonTip( 10000 );
		}
		return Retval;
	}

	private void ShowImportErrors( bool isOk, string errorText )
	{
		if( isOk )
		{
			const string TXT = "Imported with no errors";

			if( !ShowBalloon( TXT ) )
				MessageBox.Show( TXT, @"Import Ok", MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
		}
		else
			new ErrorsForm().Show( errorText );
	}

	private void ManualImportBtn_Click( object sender, EventArgs e )
	{
		ManualImportBtn.Enabled = false;
		Application.DoEvents();

		try
		{
			var Dir = ManualImportPath.Text;
			Directory.CreateDirectory( Dir );
			OpenImportFileDialog.InitialDirectory = Dir;

			if( OpenImportFileDialog.ShowDialog() == DialogResult.OK )
			{
				try
				{
					var Csv = new FileStream( OpenImportFileDialog.FileName, FileMode.Open, FileAccess.Read );

					var ErrorPth = ErrorPath.Text;

					using var Csv1 = FixCore( GetServer(), ErrorPth, Csv );

					try
					{
						using var Import = new SageImport( SageDatabase.Text, ErrorPth,
														   UserName.Text, Password.Text,
														   SalesLedger.Text, FuelSurchargeLedger.Text,
														   InventoryTypeToInventoryCodeType(),
														   IgnoreCache.Checked );

						var IsOk = Import.ImportCsv( DontUpdateAddresses.Checked, Csv1, DebugMode.Checked,
													 ErrorPth, DatabasePath.Text, out var HighestInvoiceNumber,
													 (int)EndingInvoiceNumber.Value, out var ErrorText, UpdateProjects.Checked );

						LastInvoiceNumber.Value = Math.Max( LastInvoiceNumber.Value, HighestInvoiceNumber );
						SaveSettings();

						ShowImportErrors( IsOk, ErrorText );
					}
					catch( Exception E )
					{
						MessageBox.Show( E.Message, @"Error", MessageBoxButtons.OK );
					}
				}
				catch
				{
					MessageBox.Show( @"Cannot open import file", @"Error", MessageBoxButtons.OK );
				}
			}
		}
		catch( Exception E )
		{
			MessageBox.Show( E.Message, @"Error", MessageBoxButtons.OK );
		}
		finally
		{
			IgnoreCache.Checked     = false;
			ManualImportBtn.Enabled = true;
		}
	}

	private void Button4_Click( object sender, EventArgs e )
	{
		FolderBrowserDialog.SelectedPath = ManualImportPath.Text;

		if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
			ManualImportPath.Text = FolderBrowserDialog.SelectedPath;
	}

	private string CustomerPlaceHolder() => "CustomCSVExportPlaceHolder" + AccountId.Text.Trim();

	private IdsSage.INVENTORY_CODE_TYPE InventoryTypeToInventoryCodeType()
	{
		return Provider.Text.Trim() switch
			   {
				   "Driver Name"                 => IdsSage.INVENTORY_CODE_TYPE.BY_DRIVER,
				   "Package Type - Service Type" => IdsSage.INVENTORY_CODE_TYPE.BY_PACKAGE_SERVICE,
				   _                             => IdsSage.INVENTORY_CODE_TYPE.BY_SERVICE_PACKAGE
			   };
	}

	private Stream FixCore( Communications.SERVER server, string errorPath, Stream csv )
	{
		if( server == Communications.SERVER.CORE )
		{
			using var Stream = SageImport.DebugStream( errorPath, "Pre-Phase 1 -- Data Imported From Core" );
			csv.CopyTo( Stream );
			csv.Position = 0;

			var CsvText = new StreamReader( csv ).ReadToEnd();

			var HeaderEnd = CsvText.IndexOf( "\r\n", StringComparison.Ordinal );

			// Get the header line
			var Header = CsvText.Substring( 0, HeaderEnd );

			string StripQuotes( string s )
			{
				var Length = s.Length;

				if( ( Length >= 2 ) && ( s[ 0 ] == '"' ) && ( s[ Length - 1 ] == '"' ) )
					s = s.Substring( 1, Length - 2 );

				return s.Trim();
			}

			// Remove the header line
			CsvText = CsvText.Substring( HeaderEnd + 2 );

			var Charges = Header.Split( ',' )
								.Select( StripQuotes )
								.ToArray();

			var Csv1 = new Csv( CsvText );

			foreach( Row Row in Csv1 )
			{
				void FixTime( IdsReMap.FIELDS index )
				{
					var Ndx = (int)index;
					Row[ Ndx ].AsString = $"{Row[ Ndx ].AsString}:00";
				}

				FixTime( IdsReMap.FIELDS.IDS_CALL_TIME );
				FixTime( IdsReMap.FIELDS.IDS_PICKUP_TIME );
				FixTime( IdsReMap.FIELDS.IDS_DELEVERY_TIME );

				for( int I = (int)IdsReMap.FIELDS.IDS_BEGIN_CHARGES, Limit = Charges.Length;
					 I < Limit;
					 I++ )
				{
					var Cell = Row[ I ];
					var Val  = StripQuotes( Cell.AsString );
					Cell.AsString = $"CHG_{Charges[ I ]}::{Val}";
				}
			}

			CsvText = Csv1.ToString();
			csv     = new MemoryStream( Encoding.UTF8.GetBytes( CsvText ) );
		}
		return csv;
	}

	private Communications.SERVER GetServer() => CoreServer.Checked    ? Communications.SERVER.CORE :
												 PrimaryServer.Checked ? Communications.SERVER.PRIMARY : Communications.SERVER.SECONDARY;

	private async void GetInvoicesBtn_Click( object sender, EventArgs e )
	{
		GetInvoicesBtn.Enabled = false;
		var Processing = true;

		using var Import = new SageImport( SageDatabase.Text, ErrorPath.Text,
										   UserName.Text, Password.Text,
										   SalesLedger.Text, FuelSurchargeLedger.Text,
										   InventoryTypeToInventoryCodeType(),
										   IgnoreCache.Checked );

		var HighestInvoiceNumber = -1;
		var ErrorText            = "";
		var IsOk                 = false;
		var NothingToImport      = false;

		var Server = GetServer();

		try
		{
			var Coms = new Communications( Server,
										   CarrierId.Text.Trim(),
										   AccountId.Text.Trim(),
										   UserId.Text.Trim(),
										   IdsPassword.Text,
										   (int)LastInvoiceNumber.Value,
										   CustomerPlaceHolder()
										 );

			var Debug  = DebugMode.Checked;
			var ErrP   = ErrorPath.Text;
			var DbPath = DatabasePath.Text;

			await Coms.GetCsv( csv =>
							   {
								   if( csv.Length > 0 )
								   {
									   csv = FixCore( Server, ErrP, csv );

									   // ReSharper disable once AccessToDisposedClosure
									   IsOk = Import.ImportCsv( DontUpdateAddresses.Checked, IdsReMap.ReMapCsv( csv, Debug, ErrP ), Debug, ErrP, DbPath, out HighestInvoiceNumber,
																(int)EndingInvoiceNumber.Value, out ErrorText, UpdateProjects.Checked );
								   }
								   else
									   NothingToImport = true;

								   Processing = false;
							   }, errorText =>
								  {
									  Processing = false;
									  IsOk       = false;
									  ErrorText  = errorText;
								  }
							 );
		}
		catch( Exception E )
		{
			IsOk      = Processing = false;
			ErrorText = E.Message;
		}
		finally
		{
			// ReSharper disable once LoopVariableIsNeverChangedInsideLoop
			while( Processing )
			{
				Thread.Sleep( 100 );
				Application.DoEvents();
			}

			LastInvoiceNumber.Value = Math.Max( LastInvoiceNumber.Value, HighestInvoiceNumber );
			SaveSettings();

			const string TXT = "No invoices available for import";

			if( NothingToImport && !ShowBalloon( TXT ) )
				MessageBox.Show( TXT, @"Nothing to import", MessageBoxButtons.OK, MessageBoxIcon.Information );
			else
				ShowImportErrors( IsOk, ErrorText );

			IgnoreCache.Checked    = false;
			GetInvoicesBtn.Enabled = true;
		}
	}

	private void EmptyInternalCache_Click( object sender, EventArgs e )
	{
		if( MessageBox.Show( @"Are you really sure you wish to empty the internal cache?", @"Warning", MessageBoxButtons.OKCancel ) == DialogResult.OK )
		{
			var Db = new DbCustomers();
			Db.Open( DatabasePath.Text );

			try
			{
				Db.EmptyTable();
			}
			finally
			{
				Db.Close();
			}
		}
	}

	private void Button3_Click( object sender, EventArgs e )
	{
		FolderBrowserDialog.SelectedPath = ErrorPath.Text;

		if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
			ErrorPath.Text = FolderBrowserDialog.SelectedPath;
	}

	private void Button5_Click( object sender, EventArgs e )
	{
		FolderBrowserDialog.SelectedPath = DatabasePath.Text;

		if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
			DatabasePath.Text = FolderBrowserDialog.SelectedPath;
	}

	private void NumericKeyPress( object sender, KeyPressEventArgs e )
	{
		if( !char.IsDigit( e.KeyChar ) && ( e.KeyChar != (char)Keys.Back ) )
		{
			SystemSounds.Beep.Play();
			e.Handled = true;
		}
	}

	private void MainForm_Resize( object sender, EventArgs e )
	{
		switch( WindowState )
		{
		case FormWindowState.Minimized:
			TrayIcon.Visible  = true;
			ShowInTaskbar     = false;
			PollTimer.Enabled = true;
			break;

		default:
			TrayIcon.Visible  = false;
			ShowInTaskbar     = true;
			PollTimer.Enabled = false;
			break;
		}
	}

	private void ShowToolStripMenuItem_Click( object sender, EventArgs e )
	{
		ShowInTaskbar    = true;
		TrayIcon.Visible = false;
		WindowState      = FormWindowState.Normal; // Leave Last, Stops Flicker
	}

	private void PollTimer_Tick( object sender, EventArgs e )
	{
		void UiMethod()
		{
			PollTimer.Enabled = false;

			try
			{
				var NextPol = NextPollTime.Value;

				if( NextPol < DateTime.Now )
				{
					NextPollTime.Value = NextPol.AddHours( (double)NextPollInterval.Value );
					SaveSettings();
					ShowBalloon( "Fetching Invoices." );
					GetInvoicesBtn_Click( sender, e );
				}
			}
			finally
			{
				PollTimer.Enabled = true;
			}
		}

		Invoke( (MethodInvoker)UiMethod );
	}

	private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
	{
		if( !AllowClose )
		{
			if( MessageBox.Show( @"Minimise to the icon tray?", @"Hide", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes )
			{
				e.Cancel    = true;
				WindowState = FormWindowState.Minimized;
			}
		}
	}

	private void TrayIcon_Click( object sender, EventArgs e )
	{
		WindowState = FormWindowState.Normal;
	}
}