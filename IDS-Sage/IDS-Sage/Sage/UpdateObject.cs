﻿using System.Globalization;
// ReSharper disable NotAccessedField.Global

// ReSharper disable MemberCanBePrivate.Global

namespace IDS_Sage.Import
{
	internal partial class IdsSage : Disposable
	{
		internal class UpdateObject
		{
			internal readonly TripDetailsList AccountInfo;

			private static string ConvertAndFormatDate( string dT, out DateTime Time )
			{
				try
				{
					try
					{
						Time = DateTime.ParseExact( dT, @"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture );
						return Time.ToString( "g" );
					}
					catch
					{
						Time = DateTime.ParseExact( dT, @"yyyy/MM/dd HH:mm", CultureInfo.InvariantCulture );
						return Time.ToString( "g" );
					}
				}
				catch
				{
					Time = DateTime.ParseExact( dT, @"yyyy/MM/dd", CultureInfo.InvariantCulture );
					return Time.ToString( "d" );
				}
			}

			// ReSharper disable once UnusedParameter.Local
			internal UpdateObject( INVENTORY_CODE_TYPE iType, TripDetailsList accountInfo )
			{
				AccountInfo = accountInfo;
			}

			internal class ChargeDetails
			{
				internal readonly string Description;
				internal readonly double Value;

				internal ChargeDetails( string description, string value )
				{
					Description = description;

					if( !double.TryParse( value, out Value ) )
						throw new ArgumentException( "Charge value must be numeric" );
				}
			}

			internal class TripPackageDetails
			{
				internal readonly string ServiceLevel,
										 PackageType,
										 Quantity,
										 Weight,
										 Price,
										 Extension;

				internal readonly double dQuantity,
										 dWeight,
										 dPrice,
										 dExtension;

				internal readonly string TaxDesc;
				internal readonly double TaxValue;
				internal readonly string FuelSurchargeDesc;
				internal readonly double FuelSurchargeValue;
				internal readonly List< ChargeDetails > Charges = new List< ChargeDetails >();

				internal TripPackageDetails( string serviceLevel, string packageType,
											 string quantity, string weight, string price, string extension,
											 string tax1Desc, string tax1Value,
											 string tax2Desc, string tax2Value,
											 string fuelSurchargeDesc, string fuelSurchargeValue )
				{
					ServiceLevel = serviceLevel;
					PackageType = packageType;

					Quantity = quantity;
					Weight = weight;
					Price = price;
					Extension = extension;

					if( !double.TryParse( quantity, out dQuantity ) )
						dQuantity = 0;

					if( !double.TryParse( Weight, out dWeight ) )
						dWeight = 0;

					if( !double.TryParse( price, out dPrice ) )
						throw new ArgumentException( "Trip price must be numeric" );

					if( !double.TryParse( extension, out dExtension ) )
						throw new ArgumentException( "Trip extension must be numeric" );

					TaxDesc = ( tax1Desc.Trim() != "" ? tax1Desc : tax2Desc );

					double T1v;
					if( !double.TryParse( tax1Value, out T1v ) )
						T1v = 0;

					double T2v;
					if( !double.TryParse( tax2Value, out T2v ) )
						T2v = 0;

					TaxValue = T1v + T2v;

					FuelSurchargeDesc = fuelSurchargeDesc;
					if( !double.TryParse( fuelSurchargeValue, out FuelSurchargeValue ) )
						FuelSurchargeValue = 0;
				}
			}

			internal class TripDetails
			{
				internal readonly List< TripPackageDetails > Packages = new List< TripPackageDetails >();

				internal struct AddressDetails
				{
					internal DateTime Time;

					internal string FormattedDateTime,
									DateAndTime;

					internal readonly string Company;

					internal string Reference,
									Address1,
									Address2;

					internal readonly string City,
											 Region,
											 PostCode,
											 Country,
											 Contact,
											 Telephone,
											 Email,
											 Notes;

					internal AddressDetails( string dateTime, string company, string reference,
											 string address1, string address2, string city, string region, string postCode, string country,
											 string contact, string telephone, string email, string notes )
					{
						dateTime = dateTime.Trim();
						FormattedDateTime = ConvertAndFormatDate( dateTime, out Time );

						DateAndTime = dateTime;
						Company = company.Trim();
						Reference = reference.Trim();
						Address1 = address1.Trim();
						Address2 = address2.Trim();
						City = city.Trim();
						Region = region.Trim();
						PostCode = postCode.Trim();
						Country = country.Trim();
						Contact = contact.Trim();
						Telephone = telephone.Trim();
						Email = email.Trim();
						Notes = notes.Trim();
					}
				}

				internal string AccountId;

				internal readonly string InvoiceNumber,
										 TripId,
										 TrailerDriver,
										 DriverName,
										 InvoiceDate,
										 WayBill;

				internal AddressDetails Pickup,
										Delivered,
										Billing,
										Shipping;

				internal TripDetails( string accountId, string invoiceNumber, string invoiceDate,
									  string tripId, string driverName, string trailerDriver,
									  string puDateTime, string puCompany, string puReference,
									  string puAddress1, string puAddress2, string puCity, string puRegion, string puPostCode, string puCountry,
									  string puContact, string puTelephone, string puEmail, string puNotes,
									  string delDateTime, string delCompany, string delReference,
									  string delAddress1, string delAddress2, string delCity, string delRegion, string delPostCode, string delCountry,
									  string delContact, string delTelephone, string delEmail, string delNotes,
									  string billCompany,
									  string billAddress1, string billAddress2, string billCity, string billRegion, string billPostCode, string billCountry,
									  string billContact, string billTelephone, string billEmail,
									  string shippingCompany,
									  string shippingAddress1, string shippingAddress2, string shippingCity, string shippingRegion, string shippingPostCode,
									  string shippingCountry,
									  string shippingContact, string shippingTelephone, string shippingEmail,
									  string wayBill
					)
				{
					AccountId = accountId.Trim();
					InvoiceNumber = invoiceNumber.Trim();
					InvoiceDate = invoiceDate.Trim();

					TripId = tripId.Trim();

					TrailerDriver = trailerDriver.Trim();
					DriverName = driverName.Trim();

					WayBill = wayBill;

					Pickup = new AddressDetails( puDateTime, puCompany, puReference,
												 puAddress1, puAddress2, puCity, puRegion, puPostCode, puCountry,
												 puContact, puTelephone, puEmail, puNotes );

					Delivered = new AddressDetails( delDateTime, delCompany, delReference,
													delAddress1, delAddress2, delCity, delRegion, delPostCode, delCountry,
													delContact, delTelephone, delEmail, delNotes );

					Billing = new AddressDetails( puDateTime, billCompany, puReference,
												  billAddress1, billAddress2, billCity, billRegion, billPostCode, billCountry,
												  billContact, billTelephone, billEmail, "" );

					Shipping = new AddressDetails( DateTime.Now.ToString( "yyyy-MM-dd HH:mm:ss" ), shippingCompany, "",
												   shippingAddress1, shippingAddress2, shippingCity, shippingRegion, shippingPostCode, shippingCountry,
												   shippingContact, shippingTelephone, shippingEmail, "" );
				}
			}

			internal class TripDetailsList : List< TripDetails >
			{
			}
		}
	}
}