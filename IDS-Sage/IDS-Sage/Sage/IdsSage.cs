﻿using System.Globalization;
using Simply.Domain.Utility;
using SimplySDK.ProjectModule;
using SimplySDK.Support;

// ReSharper disable AccessToModifiedClosure

namespace IDS_Sage.Import
{
	internal partial class IdsSage
	{
		internal enum INVENTORY_CODE_TYPE
		{
			BY_SERVICE_PACKAGE,
			BY_PACKAGE_SERVICE,
			BY_DRIVER
		}

		private readonly  INVENTORY_CODE_TYPE InventoryCodeType;
		private readonly  bool                ByDriver;
		private readonly  int                 SalesLedger;
		private readonly  int                 FuelSurchargeLedger;
 #pragma warning disable CS0649 // Field is never assigned to, and will always have its default value
		private readonly short AccountNumberLength;
 #pragma warning restore CS0649 // Field is never assigned to, and will always have its default value
		internal readonly bool                IgnoreCache;

		public class AlwaysYesAlert : SDKAlert
		{
			public override AlertResult AskAlert( SimplyMessage message )
			{
				return AlertResult.YES;
			}
		}

		protected override void OnDispose( bool systemDisposing )
		{
			SDKInstanceManager.Instance.CloseDatabase();
		}

		protected IdsSage( string              dbPath,            string userName, string password, string salesLedger, string fuelSurchargeLedger,
						   INVENTORY_CODE_TYPE inventoryCodeType, bool   ignoreCache )
		{
			var DbPath   = dbPath.TrimEnd();
			var UserName = userName.Trim();
			InventoryCodeType = inventoryCodeType;

			IgnoreCache = ignoreCache;

			ByDriver = InventoryCodeType == INVENTORY_CODE_TYPE.BY_DRIVER;

			if( !int.TryParse( salesLedger.Trim(), out SalesLedger ) )
				throw new ArgumentException( "Sales ledger must be numeric." );

			if( !int.TryParse( fuelSurchargeLedger.Trim(), out FuelSurchargeLedger ) )
				throw new ArgumentException( "Fuel surcharge ledger must be numeric." );
		#if !NO_SAGE
			if( !SDKInstanceManager.Instance.OpenDatabase( DbPath, UserName.Trim(), password, true, "Ids-Sage-Import", "IDS", 1 ) )
				throw new Exception( "Cannot open Sage database." );

			AccountNumberLength = (short)new SDKDatabaseUtility().RunScalerQuery( "SELECT nActNumLen FROM tCompOth" );

			SDKInstanceManager.Instance.SetAlertImplementation( new AlwaysYesAlert() );
		#endif
		}

		private static int MakeAccountNumber( int number, short maxLen )
		{
			var Zeros  = string.Empty;
			var NumStr = number <= 0 ? "1" : Convert.ToString( number );

			for( var I = NumStr.Length; I < Math.Min( (short)8, maxLen ); I++ )
				Zeros += "0";

			return Convert.ToInt32( NumStr + Zeros );
		}

		private int MakeAccountNumber( int number )
		{
			return MakeAccountNumber( number, AccountNumberLength );
		}

		private static string TrimIt( string str, int maxLen )
		{
			str = str.Trim();

			if( str.Length > maxLen )
				str = str.Substring( 0, maxLen );
			return str;
		}

		internal static void UpdateCustomer( string oldCustKey, Customer cust )
		{
			var CustLedger = SDKInstanceManager.Instance.OpenCustomerLedger();

			try
			{
				if( !CustLedger.LoadByName( oldCustKey ) )
					CustLedger.InitializeNew();

				CustLedger.Name = cust.CustomerName;

				if( cust.AddressLine1 == "" ) // No Suite
				{
					cust.AddressLine1 = cust.AddressLine2;
					cust.AddressLine2 = "";
				}

				CustLedger.Street1    = TrimIt( cust.AddressLine1, 50 );
				CustLedger.Street2    = TrimIt( cust.AddressLine2, 50 );
				CustLedger.City       = TrimIt( cust.City, 35 );
				CustLedger.Province   = TrimIt( cust.Region, 20 );
				CustLedger.PostalCode = TrimIt( cust.PostCode, 9 );
				CustLedger.Country    = TrimIt( cust.Country, 30 );

				CustLedger.Save();
			}
			catch
			{
				CustLedger.Undo();
				throw;
			}
			finally
			{
				SDKInstanceManager.Instance.CloseCustomerLedger();
			}
		}

		private string BuildInventoryKey( UpdateObject.TripDetails trip, UpdateObject.TripPackageDetails package )
		{
			return InventoryCodeType switch
				   {
					   INVENTORY_CODE_TYPE.BY_DRIVER          => trip.DriverName.Trim(),
					   INVENTORY_CODE_TYPE.BY_PACKAGE_SERVICE => ( package.PackageType + " - " + package.ServiceLevel ).Trim(),
					   _                                      => ( package.ServiceLevel + " - " + package.PackageType ).Trim()
				   };
		}

		protected void Update( UpdateObject updateData, bool updateProject )
		{
			ProjectLedger      ProjectLedger = null;
			SDKDatabaseUtility Util          = null;

			var ProjectNumbers = new Dictionary<string, string>();

			if( updateProject )
			{
				ProjectLedger = SDKInstanceManager.Instance.OpenProjectLedger();
				Util          = new SDKDatabaseUtility();
			}

			try
			{
				var CustLedger = SDKInstanceManager.Instance.OpenCustomerLedger();

				try
				{
					var AccountInfo    = updateData.AccountInfo;
					var PrimaryAccount = AccountInfo[ 0 ];

					// Billing Company Assumed To Be First In List
					var Billing  = PrimaryAccount.Billing;
					var CustCode = Billing.Company;

					if( !CustLedger.LoadByName( CustCode ) )
					{
						if( !ByDriver )
						{
							CustLedger.InitializeNew();
							CustLedger.Name = CustCode;

							if( Billing.Address1 == "" ) // No Suite
							{
								Billing.Address1 = Billing.Address2;
								Billing.Address2 = "";
							}

							CustLedger.Street1    = TrimIt( Billing.Address1, 50 );
							CustLedger.Street2    = TrimIt( Billing.Address2, 50 );
							CustLedger.City       = TrimIt( Billing.City, 35 );
							CustLedger.Province   = TrimIt( Billing.Region, 20 );
							CustLedger.PostalCode = TrimIt( Billing.PostCode, 9 );
							CustLedger.Country    = TrimIt( Billing.Country, 30 );

							CustLedger.Save();
						}
						else
							throw new Exception( "Missing Sage Customer Code : " + CustCode );
					}

					var InvLedger = SDKInstanceManager.Instance.OpenInventoryLedger();

					try
					{
						void CheckInventory( string itemCode, bool isFuelSurcharge )
						{
							if( !InvLedger.LoadByPartCode( itemCode ) )
							{
								if( !ByDriver || isFuelSurcharge )
								{
									InvLedger.InitializeNew();
									InvLedger.Number         = itemCode;
									InvLedger.Name           = "";
									InvLedger.StockingUnit   = "Each";
									InvLedger.IsServiceType  = true;
									InvLedger.RevenueAccount = MakeAccountNumber( isFuelSurcharge ? FuelSurchargeLedger : SalesLedger ).ToString();
									InvLedger.Save();
								}
								else
									throw new Exception( "Missing Sage Driver Code : " + itemCode );
							}
							else if( InvLedger.RevenueAccount.Trim() == "" )
							{
								InvLedger.RevenueAccount = MakeAccountNumber( SalesLedger ).ToString();
								InvLedger.Save();
							}
						}

						// Invoice
						var SalesJournal = SDKInstanceManager.Instance.OpenSalesJournal();

						try
						{
							SalesJournal.SelectTransType( 0 ); // invoice

							try
							{
								SalesJournal.SelectAPARLedger( CustCode );
							}
							catch( Exception E )
							{
								if( E.Message.IndexOf( "This customer often pays late.", StringComparison.Ordinal ) < 0 )
									throw;
							}

							var Line = 1;

							var Ref = PrimaryAccount.InvoiceNumber.Trim();

							if( Ref != "" )
								SalesJournal.InvoiceNumber = Ref;

							SalesJournal.SetJournalDate( PrimaryAccount.InvoiceDate );

							var ShipTo = PrimaryAccount.Shipping;

							var ShipLineNumber = 1;

							var Adr = ShipTo.Company.Trim();

							if( Adr != "" )
								SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );

							Adr = ShipTo.Address1.Trim();

							if( Adr != "" )
								SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );

							Adr = ShipTo.Address2.Trim();

							if( Adr != "" )
								SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );

							SalesJournal.SetShipToAddressLine( TrimIt( ShipTo.City, 35 ), ShipLineNumber++ );

							Adr = ShipTo.Region + "  " + ShipTo.PostCode;
							SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );
							SalesJournal.SetShipToAddressLine( TrimIt( ShipTo.Country, 30 ), ShipLineNumber );

							void BuildAddress( string preFix, UpdateObject.TripDetails.AddressDetails addr )
							{
								SalesJournal.SetDescription( "", Line++ );

								var Description = preFix + " Address: " + addr.Company;
								SalesJournal.SetDescription( Description, Line++ );

								var Temp = addr.Address1.Trim();

								if( Temp != "" )
									SalesJournal.SetDescription( Temp, Line++ );

								Temp = addr.Address2.Trim();

								if( Temp != "" )
									SalesJournal.SetDescription( Temp, Line++ );

								Description = addr.City + "  " + addr.Region + "  " + addr.PostCode;
								SalesJournal.SetDescription( Description, Line++ );

								Temp = addr.Country.Trim();

								if( Temp != "" )
									SalesJournal.SetDescription( Temp, Line++ );

								Description = "";
								Temp        = addr.Contact.Trim();

								if( Temp != "" )
									Description = Temp;

								Temp = addr.Telephone.Trim();

								if( Temp != "" )
									Description += "  " + Temp;

								if( Description != "" )
									SalesJournal.SetDescription( Description, Line++ );
							}

							double TotalTax    = 0;
							var    TaxAdjTaxId = "";

							foreach( var Trip in AccountInfo )
							{
								var ProjectReference = "";

								if( updateProject )
								{
									ProjectReference = Trip.WayBill.Trim();
									var P = ProjectReference.IndexOf( ' ' );

									if( P >= 0 )
										ProjectReference = ProjectReference.Substring( 0, P );

									if( ProjectReference == "" )
										ProjectReference = "Missing Projects";
									else
									{
										if( !ProjectNumbers.ContainsKey( ProjectReference ) )
										{
											var Count = Util.RunSelectQuery( "SELECT sName FROM tProject WHERE sName LIKE '" + ProjectReference + " %'" ); // Must have space before %
											ProjectNumbers[ ProjectReference ] = Count > 0 ? Util.GetStringFromLastSelectQuery( 0, 0 ) : ProjectReference;
										}
										ProjectReference = ProjectNumbers[ ProjectReference ];
									}

									if( !ProjectLedger.LoadByName( ProjectReference ) )
									{
										ProjectLedger.InitializeNew();
										ProjectLedger.Name = ProjectReference;
										ProjectLedger.Save();
									}
								}

								SalesJournal.SetDescription( "Trip ID: " + Trip.TripId, Line++ );
								SalesJournal.SetDescription( "Waybill: " + Trip.WayBill, Line++ );

								if( !ByDriver && ( Trip.DriverName != "" ) )
								{
									var Details = "Driver: " + Trip.DriverName;
									SalesJournal.SetDescription( Details, Line++ );

									Details = "  Pickup Time : " + Trip.Pickup.Time.ToString( CultureInfo.CurrentCulture.DateTimeFormat );
									SalesJournal.SetDescription( Details, Line++ );

									Details = "  Delivery Time : " + Trip.Delivered.Time.ToString( CultureInfo.CurrentCulture.DateTimeFormat );
									SalesJournal.SetDescription( Details, Line++ );

									BuildAddress( "Pickup", Trip.Pickup );
									BuildAddress( "Delivery", Trip.Delivered );
								}

								foreach( var Package in Trip.Packages )
								{
									TotalTax += Package.TaxValue;

									var Code = BuildInventoryKey( Trip, Package );

									CheckInventory( Code, false );

									SalesJournal.SetItemNumber( Code, Line );
									SalesJournal.SetDescription( InvLedger.Name, Line );
									SalesJournal.SetQuantity( ByDriver ? 1 : Package.dQuantity, Line );
									SalesJournal.SetUnit( InvLedger.StockingUnit, Line );

									// ReSharper disable once CompareOfFloatsByEqualityOperator
									var TripTaxId = Package.TaxValue == 0 ? "E" : Package.TaxDesc;

									try
									{
										SalesJournal.SetTaxCodeString( TripTaxId, Line );
									}
									catch
									{
										try
										{
											TripTaxId = TripTaxId.Substring( 0, 1 );
											SalesJournal.SetTaxCodeString( TripTaxId, Line );
										}
										catch
										{
											throw new Exception( "Unknown Tax Id: '" + TripTaxId + "'" );
										}
									}

									if( TaxAdjTaxId == "" )
										TaxAdjTaxId = TripTaxId;

									double ChargeTotal = 0;

									if( ByDriver ) // Sum Charges
									{
										foreach( var Charge in Package.Charges )
											ChargeTotal += Charge.Value;
									}

									SalesJournal.SetPrice( Package.dPrice, Line );

									var TotalAmount = Package.dExtension + ChargeTotal;
									SalesJournal.SetLineAmount( TotalAmount, Line );

									void AllocateProject()
									{
										if( updateProject )
										{
											try
											{
												// allocate projects
												var ProjectAllocation = SalesJournal.AllocateLine( Line );

												try
												{
													ProjectAllocation.AllocateToAll = false;
												}
												catch( Exception E )
												{
													if( !E.Message.Contains( "The current allocation will apply only to the current entry" ) )
														throw;
												}

												try
												{
													ProjectAllocation.SetProject( ProjectReference, 1 );
													ProjectAllocation.SetPercent( 100, 1 );
													ProjectAllocation.Save();
												}
												catch
												{
													ProjectAllocation.Cancel();
													throw;
												}
											}
											catch( Exception E )
											{
												if( !E.Message.Contains( "Entry must have an amount" ) )
													throw;
											}
										}
										Line++;
									}

									AllocateProject();

									if( !ByDriver )
									{
										foreach( var Charge in Package.Charges )
										{
											var Value = Charge.Value;

											// ReSharper disable once CompareOfFloatsByEqualityOperator
											if( Value != 0 )
											{
												var Desc = "CHARGE_" + Charge.Description.Trim();
												CheckInventory( Desc, false );
												SalesJournal.SetItemNumber( Desc, Line );
												SalesJournal.SetDescription( InvLedger.Name, Line );
												SalesJournal.SetUnit( InvLedger.StockingUnit, Line );
												SalesJournal.SetQuantity( 1, Line );
												SalesJournal.SetLineAmount( Value, Line );
												SalesJournal.SetTaxCodeString( TripTaxId, Line );

												AllocateProject();
											}
										}
									}

									// ReSharper disable once CompareOfFloatsByEqualityOperator
									if( Package.FuelSurchargeValue != 0 )
									{
										var Desc = "CHARGE_" + Package.FuelSurchargeDesc;
										CheckInventory( Desc, true );
										SalesJournal.SetItemNumber( Desc, Line );
										SalesJournal.SetDescription( InvLedger.Name, Line );
										SalesJournal.SetUnit( InvLedger.StockingUnit, Line );
										SalesJournal.SetQuantity( 1, Line );
										SalesJournal.SetLineAmount( Package.FuelSurchargeValue, Line );
										SalesJournal.SetTaxCodeString( TripTaxId, Line );

										AllocateProject();
									}
								}
							}

							var SageTotalTax = SalesJournal.GetTaxTotalAmount();

							TotalTax = Math.Round( TotalTax, 2, MidpointRounding.AwayFromZero );

							if( Math.Abs( SageTotalTax - TotalTax ) > 0.0001 )
							{
								try
								{
									SalesJournal.SetFreightTaxCode( "E" );
									var TaxDiff = Math.Round( TotalTax - SageTotalTax, 2, MidpointRounding.AwayFromZero );

									try
									{
										SalesJournal.SetFreightTax1Amount( TaxDiff );
									}
									catch // Can't set customer record has tax rated
									{
										SalesJournal.SetFreightAmount( TaxDiff );
									}
								}
								catch
								{
									throw new Exception( "Cannot post freight adjustment (Missing freight linkage?)" );
								}
							}

							try
							{
								if( !SalesJournal.Post() )
									throw new IOException( "Cannot post sale." );
							}
							catch( Exception E )
							{
								if( ( E.Message.IndexOf( "processed successfully", StringComparison.Ordinal ) < 0 )
									&& ( E.Message.IndexOf( "Fast Posting", StringComparison.Ordinal ) < 0 )
									&& ( E.Message.IndexOf( "This customer often pays late.", StringComparison.Ordinal ) < 0 ) )
									throw;
							}
						}
						catch
						{
							SalesJournal.Undo();
							throw;
						}
						finally
						{
							SDKInstanceManager.Instance.CloseSalesJournal();
						}
					}
					catch
					{
						InvLedger.Undo();
						throw;
					}
					finally
					{
						SDKInstanceManager.Instance.CloseInventoryLedger();
					}
				}
				catch
				{
					CustLedger.Undo();
					throw;
				}
				finally
				{
					SDKInstanceManager.Instance.CloseCustomerLedger();
				}
			}
			finally
			{
				if( updateProject )
					SDKInstanceManager.Instance.CloseProjectLedger();
			}
		}
	}
}