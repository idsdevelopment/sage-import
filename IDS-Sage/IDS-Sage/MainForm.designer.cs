﻿namespace IDS_Sage
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.CloseSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.MainTabControl = new System.Windows.Forms.TabControl();
			this.StatusTabPage = new System.Windows.Forms.TabPage();
			this.label17 = new System.Windows.Forms.Label();
			this.NextPollInterval = new System.Windows.Forms.NumericUpDown();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.NextPollTime = new System.Windows.Forms.DateTimePicker();
			this.GetInvoicesBtn = new System.Windows.Forms.Button();
			this.ManualImportBtn = new System.Windows.Forms.Button();
			this.ImportSettingsTabPage = new System.Windows.Forms.TabPage();
			this.IgnoreCache = new System.Windows.Forms.CheckBox();
			this.button5 = new System.Windows.Forms.Button();
			this.label13 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.DeleteInternalCacheBtn = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.CoreServer = new System.Windows.Forms.RadioButton();
			this.SecondaryServer = new System.Windows.Forms.RadioButton();
			this.PrimaryServer = new System.Windows.Forms.RadioButton();
			this.label18 = new System.Windows.Forms.Label();
			this.EndingInvoiceNumber = new System.Windows.Forms.NumericUpDown();
			this.label14 = new System.Windows.Forms.Label();
			this.Provider = new System.Windows.Forms.ComboBox();
			this.LastInvoiceNumber = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.IdsPassword = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.UserId = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.AccountId = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.CarrierId = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.DatabasePath = new System.Windows.Forms.TextBox();
			this.DebugMode = new System.Windows.Forms.CheckBox();
			this.ErrorPath = new System.Windows.Forms.TextBox();
			this.ManualImportPath = new System.Windows.Forms.TextBox();
			this.SageSettingsTabPage = new System.Windows.Forms.TabPage();
			this.DontUpdateAddresses = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.UpdateProjects = new System.Windows.Forms.CheckBox();
			this.FuelSurchargeLedger = new System.Windows.Forms.MaskedTextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.SalesLedger = new System.Windows.Forms.MaskedTextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SageDatabase = new System.Windows.Forms.TextBox();
			this.TestConnectionBtn = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.Password = new System.Windows.Forms.TextBox();
			this.UserName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.OpenImportFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.IconTrayMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.PollTimer = new System.Windows.Forms.Timer(this.components);
			this.menuStrip1.SuspendLayout();
			this.MainTabControl.SuspendLayout();
			this.StatusTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NextPollInterval)).BeginInit();
			this.ImportSettingsTabPage.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.EndingInvoiceNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LastInvoiceNumber)).BeginInit();
			this.SageSettingsTabPage.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.IconTrayMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.CloseSettings});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 1, 0, 1);
			this.menuStrip1.Size = new System.Drawing.Size(588, 31);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(54, 29);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(141, 34);
			this.exitToolStripMenuItem.Text = "&Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preferencesToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(58, 29);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// preferencesToolStripMenuItem
			// 
			this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.sageToolStripMenuItem});
			this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
			this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(204, 34);
			this.preferencesToolStripMenuItem.Text = "Preferences";
			// 
			// importToolStripMenuItem
			// 
			this.importToolStripMenuItem.Name = "importToolStripMenuItem";
			this.importToolStripMenuItem.Size = new System.Drawing.Size(222, 34);
			this.importToolStripMenuItem.Text = "IDS Settings";
			this.importToolStripMenuItem.Click += new System.EventHandler(this.ImportSettingsMenuItem_Click);
			// 
			// sageToolStripMenuItem
			// 
			this.sageToolStripMenuItem.Name = "sageToolStripMenuItem";
			this.sageToolStripMenuItem.Size = new System.Drawing.Size(222, 34);
			this.sageToolStripMenuItem.Text = "Sage Settings";
			this.sageToolStripMenuItem.Click += new System.EventHandler(this.SageToolStripMenuItem_Click);
			// 
			// CloseSettings
			// 
			this.CloseSettings.Enabled = false;
			this.CloseSettings.Name = "CloseSettings";
			this.CloseSettings.Size = new System.Drawing.Size(140, 29);
			this.CloseSettings.Text = "Close Settings";
			this.CloseSettings.Click += new System.EventHandler(this.CloseSettingsToolStripMenuItem_Click);
			// 
			// MainTabControl
			// 
			this.MainTabControl.Controls.Add(this.StatusTabPage);
			this.MainTabControl.Controls.Add(this.ImportSettingsTabPage);
			this.MainTabControl.Controls.Add(this.SageSettingsTabPage);
			this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainTabControl.Location = new System.Drawing.Point(0, 31);
			this.MainTabControl.Name = "MainTabControl";
			this.MainTabControl.SelectedIndex = 0;
			this.MainTabControl.Size = new System.Drawing.Size(588, 441);
			this.MainTabControl.TabIndex = 2;
			// 
			// StatusTabPage
			// 
			this.StatusTabPage.Controls.Add(this.label17);
			this.StatusTabPage.Controls.Add(this.NextPollInterval);
			this.StatusTabPage.Controls.Add(this.label16);
			this.StatusTabPage.Controls.Add(this.label15);
			this.StatusTabPage.Controls.Add(this.NextPollTime);
			this.StatusTabPage.Controls.Add(this.GetInvoicesBtn);
			this.StatusTabPage.Controls.Add(this.ManualImportBtn);
			this.StatusTabPage.Location = new System.Drawing.Point(4, 22);
			this.StatusTabPage.Name = "StatusTabPage";
			this.StatusTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.StatusTabPage.Size = new System.Drawing.Size(580, 415);
			this.StatusTabPage.TabIndex = 0;
			this.StatusTabPage.Text = "Status";
			this.StatusTabPage.UseVisualStyleBackColor = true;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(251, 34);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(35, 13);
			this.label17.TabIndex = 6;
			this.label17.Text = "Hours";
			// 
			// NextPollInterval
			// 
			this.NextPollInterval.Location = new System.Drawing.Point(217, 32);
			this.NextPollInterval.Name = "NextPollInterval";
			this.NextPollInterval.Size = new System.Drawing.Size(37, 20);
			this.NextPollInterval.TabIndex = 5;
			this.NextPollInterval.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(155, 34);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(66, 13);
			this.label16.TabIndex = 4;
			this.label16.Text = "Import Every";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(9, 16);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(90, 13);
			this.label15.TabIndex = 3;
			this.label15.Text = "Next Import Time:";
			// 
			// NextPollTime
			// 
			this.NextPollTime.Checked = false;
			this.NextPollTime.CustomFormat = "dd/MM/yyyy HH:mm";
			this.NextPollTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.NextPollTime.Location = new System.Drawing.Point(12, 32);
			this.NextPollTime.Name = "NextPollTime";
			this.NextPollTime.ShowUpDown = true;
			this.NextPollTime.Size = new System.Drawing.Size(115, 20);
			this.NextPollTime.TabIndex = 2;
			this.NextPollTime.Value = new System.DateTime(2014, 4, 8, 17, 15, 47, 0);
			// 
			// GetInvoicesBtn
			// 
			this.GetInvoicesBtn.Location = new System.Drawing.Point(8, 88);
			this.GetInvoicesBtn.Name = "GetInvoicesBtn";
			this.GetInvoicesBtn.Size = new System.Drawing.Size(144, 23);
			this.GetInvoicesBtn.TabIndex = 1;
			this.GetInvoicesBtn.Text = "Get Invoices From IDS";
			this.GetInvoicesBtn.UseVisualStyleBackColor = true;
			this.GetInvoicesBtn.Click += new System.EventHandler(this.GetInvoicesBtn_Click);
			// 
			// ManualImportBtn
			// 
			this.ManualImportBtn.Location = new System.Drawing.Point(445, 88);
			this.ManualImportBtn.Name = "ManualImportBtn";
			this.ManualImportBtn.Size = new System.Drawing.Size(111, 23);
			this.ManualImportBtn.TabIndex = 0;
			this.ManualImportBtn.Text = "Manual Import";
			this.ManualImportBtn.UseVisualStyleBackColor = true;
			this.ManualImportBtn.Click += new System.EventHandler(this.ManualImportBtn_Click);
			// 
			// ImportSettingsTabPage
			// 
			this.ImportSettingsTabPage.Controls.Add(this.IgnoreCache);
			this.ImportSettingsTabPage.Controls.Add(this.button5);
			this.ImportSettingsTabPage.Controls.Add(this.label13);
			this.ImportSettingsTabPage.Controls.Add(this.button3);
			this.ImportSettingsTabPage.Controls.Add(this.DeleteInternalCacheBtn);
			this.ImportSettingsTabPage.Controls.Add(this.groupBox3);
			this.ImportSettingsTabPage.Controls.Add(this.label5);
			this.ImportSettingsTabPage.Controls.Add(this.button4);
			this.ImportSettingsTabPage.Controls.Add(this.label4);
			this.ImportSettingsTabPage.Controls.Add(this.DatabasePath);
			this.ImportSettingsTabPage.Controls.Add(this.DebugMode);
			this.ImportSettingsTabPage.Controls.Add(this.ErrorPath);
			this.ImportSettingsTabPage.Controls.Add(this.ManualImportPath);
			this.ImportSettingsTabPage.Location = new System.Drawing.Point(4, 22);
			this.ImportSettingsTabPage.Name = "ImportSettingsTabPage";
			this.ImportSettingsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.ImportSettingsTabPage.Size = new System.Drawing.Size(580, 415);
			this.ImportSettingsTabPage.TabIndex = 1;
			this.ImportSettingsTabPage.Text = "IDS Settings";
			this.ImportSettingsTabPage.UseVisualStyleBackColor = true;
			// 
			// IgnoreCache
			// 
			this.IgnoreCache.AutoSize = true;
			this.IgnoreCache.Location = new System.Drawing.Point(129, 387);
			this.IgnoreCache.Name = "IgnoreCache";
			this.IgnoreCache.Size = new System.Drawing.Size(170, 21);
			this.IgnoreCache.TabIndex = 7;
			this.IgnoreCache.Text = "Ignore Cache For This Import";
			this.IgnoreCache.UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(491, 331);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 19;
			this.button5.Text = "Browse";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5_Click);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(11, 318);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(104, 13);
			this.label13.TabIndex = 17;
			this.label13.Text = "Internal Cache Path:";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(491, 277);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 15;
			this.button3.Text = "Browse";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3_Click);
			// 
			// DeleteInternalCacheBtn
			// 
			this.DeleteInternalCacheBtn.Location = new System.Drawing.Point(321, 382);
			this.DeleteInternalCacheBtn.Name = "DeleteInternalCacheBtn";
			this.DeleteInternalCacheBtn.Size = new System.Drawing.Size(142, 23);
			this.DeleteInternalCacheBtn.TabIndex = 4;
			this.DeleteInternalCacheBtn.Text = "Empty Internal Cache";
			this.DeleteInternalCacheBtn.UseVisualStyleBackColor = true;
			this.DeleteInternalCacheBtn.Click += new System.EventHandler(this.EmptyInternalCache_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.groupBox4);
			this.groupBox3.Controls.Add(this.label18);
			this.groupBox3.Controls.Add(this.EndingInvoiceNumber);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.Provider);
			this.groupBox3.Controls.Add(this.LastInvoiceNumber);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Controls.Add(this.IdsPassword);
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Controls.Add(this.UserId);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.AccountId);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.CarrierId);
			this.groupBox3.Location = new System.Drawing.Point(8, 16);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(564, 192);
			this.groupBox3.TabIndex = 13;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "IDS Account Settings";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.CoreServer);
			this.groupBox4.Controls.Add(this.SecondaryServer);
			this.groupBox4.Controls.Add(this.PrimaryServer);
			this.groupBox4.Location = new System.Drawing.Point(6, 25);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.groupBox4.Size = new System.Drawing.Size(312, 46);
			this.groupBox4.TabIndex = 32;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Server";
			// 
			// CoreServer
			// 
			this.CoreServer.AutoSize = true;
			this.CoreServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CoreServer.Location = new System.Drawing.Point(241, 17);
			this.CoreServer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.CoreServer.Name = "CoreServer";
			this.CoreServer.Size = new System.Drawing.Size(72, 24);
			this.CoreServer.TabIndex = 2;
			this.CoreServer.TabStop = true;
			this.CoreServer.Text = "Core";
			this.CoreServer.UseVisualStyleBackColor = true;
			// 
			// SecondaryServer
			// 
			this.SecondaryServer.AutoSize = true;
			this.SecondaryServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SecondaryServer.Location = new System.Drawing.Point(128, 17);
			this.SecondaryServer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.SecondaryServer.Name = "SecondaryServer";
			this.SecondaryServer.Size = new System.Drawing.Size(67, 24);
			this.SecondaryServer.TabIndex = 1;
			this.SecondaryServer.TabStop = true;
			this.SecondaryServer.Text = "East";
			this.SecondaryServer.UseVisualStyleBackColor = true;
			// 
			// PrimaryServer
			// 
			this.PrimaryServer.AutoSize = true;
			this.PrimaryServer.Checked = true;
			this.PrimaryServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrimaryServer.Location = new System.Drawing.Point(11, 17);
			this.PrimaryServer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.PrimaryServer.Name = "PrimaryServer";
			this.PrimaryServer.Size = new System.Drawing.Size(71, 24);
			this.PrimaryServer.TabIndex = 0;
			this.PrimaryServer.TabStop = true;
			this.PrimaryServer.Text = "West";
			this.PrimaryServer.UseVisualStyleBackColor = true;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(424, 127);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(118, 13);
			this.label18.TabIndex = 31;
			this.label18.Text = "Ending Invoice Number";
			// 
			// EndingInvoiceNumber
			// 
			this.EndingInvoiceNumber.Location = new System.Drawing.Point(427, 144);
			this.EndingInvoiceNumber.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
			this.EndingInvoiceNumber.Name = "EndingInvoiceNumber";
			this.EndingInvoiceNumber.Size = new System.Drawing.Size(74, 20);
			this.EndingInvoiceNumber.TabIndex = 30;
			this.EndingInvoiceNumber.Value = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(275, 83);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(128, 13);
			this.label14.TabIndex = 29;
			this.label14.Text = "Inventory Code Structure:";
			// 
			// Provider
			// 
			this.Provider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.Provider.FormattingEnabled = true;
			this.Provider.Items.AddRange(new object[] {
            "Service Type - Package Type",
            "Package Type - Service Type",
            "Driver Name"});
			this.Provider.Location = new System.Drawing.Point(278, 97);
			this.Provider.Name = "Provider";
			this.Provider.Size = new System.Drawing.Size(272, 21);
			this.Provider.TabIndex = 28;
			// 
			// LastInvoiceNumber
			// 
			this.LastInvoiceNumber.Location = new System.Drawing.Point(286, 144);
			this.LastInvoiceNumber.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
			this.LastInvoiceNumber.Name = "LastInvoiceNumber";
			this.LastInvoiceNumber.Size = new System.Drawing.Size(75, 20);
			this.LastInvoiceNumber.TabIndex = 26;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(283, 127);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(126, 13);
			this.label8.TabIndex = 27;
			this.label8.Text = "Last IDS Invoice Number";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(141, 129);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(53, 13);
			this.label12.TabIndex = 24;
			this.label12.Text = "Password";
			// 
			// IdsPassword
			// 
			this.IdsPassword.Location = new System.Drawing.Point(144, 144);
			this.IdsPassword.Name = "IdsPassword";
			this.IdsPassword.PasswordChar = '*';
			this.IdsPassword.Size = new System.Drawing.Size(100, 20);
			this.IdsPassword.TabIndex = 25;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(15, 127);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(41, 13);
			this.label11.TabIndex = 22;
			this.label11.Text = "User Id";
			// 
			// UserId
			// 
			this.UserId.Location = new System.Drawing.Point(18, 143);
			this.UserId.Name = "UserId";
			this.UserId.Size = new System.Drawing.Size(100, 20);
			this.UserId.TabIndex = 23;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(141, 83);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(59, 13);
			this.label10.TabIndex = 20;
			this.label10.Text = "Account Id";
			// 
			// AccountId
			// 
			this.AccountId.Location = new System.Drawing.Point(144, 98);
			this.AccountId.Name = "AccountId";
			this.AccountId.Size = new System.Drawing.Size(100, 20);
			this.AccountId.TabIndex = 21;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(15, 83);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(49, 13);
			this.label9.TabIndex = 18;
			this.label9.Text = "Carrier Id";
			// 
			// CarrierId
			// 
			this.CarrierId.Location = new System.Drawing.Point(18, 98);
			this.CarrierId.Name = "CarrierId";
			this.CarrierId.Size = new System.Drawing.Size(100, 20);
			this.CarrierId.TabIndex = 19;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(11, 264);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "Error Folder:";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(491, 225);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 10;
			this.button4.Text = "Browse";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(11, 212);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(99, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "Manual Import Path";
			// 
			// DatabasePath
			// 
			this.DatabasePath.Location = new System.Drawing.Point(14, 334);
			this.DatabasePath.Name = "DatabasePath";
			this.DatabasePath.Size = new System.Drawing.Size(449, 20);
			this.DatabasePath.TabIndex = 18;
			this.DatabasePath.Text = "C:\\Ids\\Cache\\";
			// 
			// DebugMode
			// 
			this.DebugMode.AutoSize = true;
			this.DebugMode.Location = new System.Drawing.Point(12, 387);
			this.DebugMode.Name = "DebugMode";
			this.DebugMode.Size = new System.Drawing.Size(95, 21);
			this.DebugMode.TabIndex = 16;
			this.DebugMode.Text = "Debug Mode";
			this.DebugMode.UseVisualStyleBackColor = true;
			// 
			// ErrorPath
			// 
			this.ErrorPath.Location = new System.Drawing.Point(14, 280);
			this.ErrorPath.Name = "ErrorPath";
			this.ErrorPath.Size = new System.Drawing.Size(449, 20);
			this.ErrorPath.TabIndex = 14;
			this.ErrorPath.Text = "C:\\Ids\\Errors";
			// 
			// ManualImportPath
			// 
			this.ManualImportPath.Location = new System.Drawing.Point(14, 227);
			this.ManualImportPath.Name = "ManualImportPath";
			this.ManualImportPath.Size = new System.Drawing.Size(449, 20);
			this.ManualImportPath.TabIndex = 8;
			this.ManualImportPath.Text = "C:\\Ids\\";
			this.ManualImportPath.Leave += new System.EventHandler(this.ManualImportPath_Leave);
			// 
			// SageSettingsTabPage
			// 
			this.SageSettingsTabPage.Controls.Add(this.DontUpdateAddresses);
			this.SageSettingsTabPage.Controls.Add(this.groupBox2);
			this.SageSettingsTabPage.Controls.Add(this.groupBox1);
			this.SageSettingsTabPage.Location = new System.Drawing.Point(4, 22);
			this.SageSettingsTabPage.Name = "SageSettingsTabPage";
			this.SageSettingsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.SageSettingsTabPage.Size = new System.Drawing.Size(580, 415);
			this.SageSettingsTabPage.TabIndex = 2;
			this.SageSettingsTabPage.Text = "Sage Settings";
			this.SageSettingsTabPage.UseVisualStyleBackColor = true;
			// 
			// DontUpdateAddresses
			// 
			this.DontUpdateAddresses.AutoSize = true;
			this.DontUpdateAddresses.Checked = global::IDS_Sage.Properties.Settings.Default.DontUpdateAddresses;
			this.DontUpdateAddresses.Location = new System.Drawing.Point(6, 267);
			this.DontUpdateAddresses.Name = "DontUpdateAddresses";
			this.DontUpdateAddresses.Size = new System.Drawing.Size(249, 21);
			this.DontUpdateAddresses.TabIndex = 28;
			this.DontUpdateAddresses.Text = "Do Not Update Sage Address Lines From IDS";
			this.DontUpdateAddresses.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.UpdateProjects);
			this.groupBox2.Controls.Add(this.FuelSurchargeLedger);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.SalesLedger);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Cursor = System.Windows.Forms.Cursors.SizeNESW;
			this.groupBox2.Location = new System.Drawing.Point(8, 152);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(463, 109);
			this.groupBox2.TabIndex = 26;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Invoice Settings";
			// 
			// UpdateProjects
			// 
			this.UpdateProjects.AutoSize = true;
			this.UpdateProjects.Checked = global::IDS_Sage.Properties.Settings.Default.UpdateProjects;
			this.UpdateProjects.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::IDS_Sage.Properties.Settings.Default, "UpdateProjects", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.UpdateProjects.Location = new System.Drawing.Point(19, 77);
			this.UpdateProjects.Name = "UpdateProjects";
			this.UpdateProjects.Size = new System.Drawing.Size(209, 21);
			this.UpdateProjects.TabIndex = 29;
			this.UpdateProjects.Text = "Update Projects From Trip Reference";
			this.UpdateProjects.UseVisualStyleBackColor = true;
			// 
			// FuelSurchargeLedger
			// 
			this.FuelSurchargeLedger.Location = new System.Drawing.Point(117, 42);
			this.FuelSurchargeLedger.Name = "FuelSurchargeLedger";
			this.FuelSurchargeLedger.Size = new System.Drawing.Size(78, 20);
			this.FuelSurchargeLedger.TabIndex = 17;
			this.FuelSurchargeLedger.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericKeyPress);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(114, 26);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(115, 13);
			this.label7.TabIndex = 16;
			this.label7.Text = "Fuel Surcharge Ledger";
			// 
			// SalesLedger
			// 
			this.SalesLedger.Location = new System.Drawing.Point(19, 42);
			this.SalesLedger.Name = "SalesLedger";
			this.SalesLedger.Size = new System.Drawing.Size(78, 20);
			this.SalesLedger.TabIndex = 15;
			this.SalesLedger.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericKeyPress);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(16, 26);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(69, 13);
			this.label6.TabIndex = 13;
			this.label6.Text = "Sales Ledger";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.SageDatabase);
			this.groupBox1.Controls.Add(this.TestConnectionBtn);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.Password);
			this.groupBox1.Controls.Add(this.UserName);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Location = new System.Drawing.Point(8, 6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(463, 140);
			this.groupBox1.TabIndex = 25;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Sage Databse Settings";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(375, 33);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 27;
			this.button1.Text = "Browse";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(16, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(81, 13);
			this.label1.TabIndex = 26;
			this.label1.Text = "Sage Database";
			// 
			// SageDatabase
			// 
			this.SageDatabase.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::IDS_Sage.Properties.Settings.Default, "SageDatabase", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.SageDatabase.Location = new System.Drawing.Point(19, 36);
			this.SageDatabase.Name = "SageDatabase";
			this.SageDatabase.Size = new System.Drawing.Size(344, 20);
			this.SageDatabase.TabIndex = 25;
			this.SageDatabase.Text = global::IDS_Sage.Properties.Settings.Default.SageDatabase;
			this.SageDatabase.Leave += new System.EventHandler(this.SageDatabase_Leave);
			// 
			// TestConnectionBtn
			// 
			this.TestConnectionBtn.Location = new System.Drawing.Point(295, 111);
			this.TestConnectionBtn.Name = "TestConnectionBtn";
			this.TestConnectionBtn.Size = new System.Drawing.Size(133, 23);
			this.TestConnectionBtn.TabIndex = 24;
			this.TestConnectionBtn.Text = "Test Connection";
			this.TestConnectionBtn.UseVisualStyleBackColor = true;
			this.TestConnectionBtn.Click += new System.EventHandler(this.TestConnectionBtn_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(16, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 20;
			this.label2.Text = "User Name";
			// 
			// Password
			// 
			this.Password.Location = new System.Drawing.Point(277, 81);
			this.Password.Name = "Password";
			this.Password.Size = new System.Drawing.Size(151, 20);
			this.Password.TabIndex = 23;
			// 
			// UserName
			// 
			this.UserName.Location = new System.Drawing.Point(19, 81);
			this.UserName.Name = "UserName";
			this.UserName.Size = new System.Drawing.Size(151, 20);
			this.UserName.TabIndex = 21;
			this.UserName.Text = "sysadmin";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(274, 65);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(53, 13);
			this.label3.TabIndex = 22;
			this.label3.Text = "Password";
			// 
			// OpenFileDialog
			// 
			this.OpenFileDialog.Filter = "Sage|*.sai";
			// 
			// OpenImportFileDialog
			// 
			this.OpenImportFileDialog.Filter = "Csv File|*.csv";
			// 
			// TrayIcon
			// 
			this.TrayIcon.ContextMenuStrip = this.IconTrayMenuStrip;
			this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
			this.TrayIcon.Text = "IDS-Sage";
			this.TrayIcon.Click += new System.EventHandler(this.TrayIcon_Click);
			// 
			// IconTrayMenuStrip
			// 
			this.IconTrayMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.IconTrayMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.showToolStripMenuItem});
			this.IconTrayMenuStrip.Name = "contextMenuStrip1";
			this.IconTrayMenuStrip.Size = new System.Drawing.Size(129, 68);
			// 
			// fileToolStripMenuItem1
			// 
			this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem1});
			this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
			this.fileToolStripMenuItem1.Size = new System.Drawing.Size(128, 32);
			this.fileToolStripMenuItem1.Text = "&File";
			// 
			// exitToolStripMenuItem1
			// 
			this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
			this.exitToolStripMenuItem1.Size = new System.Drawing.Size(141, 34);
			this.exitToolStripMenuItem1.Text = "&Exit";
			this.exitToolStripMenuItem1.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
			// 
			// showToolStripMenuItem
			// 
			this.showToolStripMenuItem.Name = "showToolStripMenuItem";
			this.showToolStripMenuItem.Size = new System.Drawing.Size(128, 32);
			this.showToolStripMenuItem.Text = "Show";
			this.showToolStripMenuItem.Click += new System.EventHandler(this.ShowToolStripMenuItem_Click);
			// 
			// FolderBrowserDialog
			// 
			this.FolderBrowserDialog.Description = "Select Path";
			// 
			// PollTimer
			// 
			this.PollTimer.Interval = 60000;
			this.PollTimer.Tick += new System.EventHandler(this.PollTimer_Tick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(588, 472);
			this.Controls.Add(this.MainTabControl);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "Ids-Sage Vsn";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.Shown += new System.EventHandler(this.MainForm_Shown);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.MainTabControl.ResumeLayout(false);
			this.StatusTabPage.ResumeLayout(false);
			this.StatusTabPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.NextPollInterval)).EndInit();
			this.ImportSettingsTabPage.ResumeLayout(false);
			this.ImportSettingsTabPage.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.EndingInvoiceNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LastInvoiceNumber)).EndInit();
			this.SageSettingsTabPage.ResumeLayout(false);
			this.SageSettingsTabPage.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.IconTrayMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.TabControl MainTabControl;
		private System.Windows.Forms.TabPage StatusTabPage;
		private System.Windows.Forms.TabPage ImportSettingsTabPage;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog OpenFileDialog;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
		private System.Windows.Forms.Button ManualImportBtn;
		private System.Windows.Forms.OpenFileDialog OpenImportFileDialog;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox ManualImportPath;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NotifyIcon TrayIcon;
		private System.Windows.Forms.MaskedTextBox SalesLedger;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem sageToolStripMenuItem;
		private System.Windows.Forms.TabPage SageSettingsTabPage;
		private System.Windows.Forms.ToolStripMenuItem CloseSettings;
		private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
		private System.Windows.Forms.Button GetInvoicesBtn;
		private System.Windows.Forms.ContextMenuStrip IconTrayMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox IdsPassword;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox UserId;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox AccountId;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox CarrierId;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button TestConnectionBtn;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Password;
		private System.Windows.Forms.TextBox UserName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox SageDatabase;
		private System.Windows.Forms.NumericUpDown LastInvoiceNumber;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button DeleteInternalCacheBtn;
		private System.Windows.Forms.TextBox ErrorPath;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.CheckBox DebugMode;
		private System.Windows.Forms.MaskedTextBox FuelSurchargeLedger;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.TextBox DatabasePath;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox Provider;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.NumericUpDown NextPollInterval;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.DateTimePicker NextPollTime;
		private System.Windows.Forms.Timer PollTimer;
		private System.Windows.Forms.CheckBox IgnoreCache;
		private System.Windows.Forms.CheckBox DontUpdateAddresses;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.NumericUpDown EndingInvoiceNumber;
		private System.Windows.Forms.CheckBox UpdateProjects;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.RadioButton SecondaryServer;
		private System.Windows.Forms.RadioButton PrimaryServer;
		private RadioButton CoreServer;
	}
}

