New URL.  Remember you have to have the name Phoenix as the placeholder in the string

http://jbtest.internetdispatcher.org:8080/ids/reports?carrierId=phoenix&accountId=phoenix&userId=lee-ann&p=enarc&report=CustomCSVExportPlaceHolderPhoenix&output=99&invoiceId=0&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true


Calculating the first inventory line:
Total including taxes - GST/HST - charge type FSC (fuel surcharge) = Extension amount for the inventory code.

Delivery Driver Username in the csv file is the inventory code

So the invoice has 2 inventory line items.

Qty                              Inventory code                              Inventory Description                        Amount                                    Tax code
1                                 Delivery Driver Username               Blank                                            Calculated as above                      G or H or GST or HST
1                                  FSC                                                Fuel Surcharge                              CHG_FSC                                    G or H or GST or HST


GST/HST at the bottom of the invoice as usual.

I can guarantee they will want something in the inventory description field.  I am not going to try guessing.

I have created a couple of invoices starting at 0.  Angus adds 1 then exports.
