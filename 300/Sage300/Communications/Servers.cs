﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Sage300.Communications
{
	public static class Servers
	{
		public const string WEST = "http://users.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true",
							EAST = "http://reports.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";

		public static string ToServer( this MainWindowViewModel.SERVER s, string carrierId, string accountId, string userId, string password, int startingInvoice )
		{
			accountId = accountId.Trim();

			var Server = s is MainWindowViewModel.SERVER.WEST ? WEST : EAST;

			return Server.Replace( "%CID%", carrierId.Trim() )
						 .Replace( "%ACID%", accountId )
						 .Replace( "%UID%", userId.Trim() )
						 .Replace( "%PASS%", password.Trim() )
						 .Replace( "%INV_ID%", startingInvoice.ToString() )
						 .Replace( "%REPORT%", $"CustomCSVExportPlaceHolder{accountId}" );
		}
	}

	public class Client : HttpClient
	{
		public async Task<(bool Ok, string Value)> GetCsv( MainWindowViewModel.SERVER s, string carrierId, string accountId, string userId, string password, int startingInvoice )
		{
			var Message = await GetAsync( s.ToServer( carrierId, accountId, userId, password, startingInvoice ) );
			Message.EnsureSuccessStatusCode();

			if( Message.IsSuccessStatusCode )
			{
				var Csv = await Message.Content.ReadAsStringAsync();

				return Csv.Contains( "Authentication Error" ) ? ( false, "Authentication Error" ) : ( true, Csv );
			}
			return ( false, $"Communications error: {Message.StatusCode}" );
		}
	}
}