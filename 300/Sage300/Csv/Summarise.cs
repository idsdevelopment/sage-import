﻿using System;
using Sage300.Globals;
using Utils.Csv;

namespace Sage300.Csv
{
	public class Summarise
	{
		public static (bool Ok, Utils.Csv.Csv Csv) SummariseCsv( Utils.Csv.Csv csv )
		{
			var Ok = false;

			try
			{
				csv.Sort( ( a, b ) =>
				          {
					          var RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ], b[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ] );

					          if( RetVal == 0 )
					          {
						          RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ], b[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ] );

						          if( RetVal == 0 )
						          {
							          RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.TRIP_ID ], b[ (int)TRIP_LINE.FIELDS.TRIP_ID ] );

							          if( RetVal == 0 )
							          {
								          var A0 = a[ 0 ];

								          RetVal = string.CompareOrdinal( A0, b[ 0 ] ); // Type

								          if( RetVal == 0 )
								          {
									          // Special charges last
									          static int Compare( string c1, string c2 )
									          {
										          static bool IsFixedCharge( string c )
										          {
											          return c.StartsWith( "Fuel", StringComparison.OrdinalIgnoreCase )
											                 || c.StartsWith( "Ferry", StringComparison.OrdinalIgnoreCase )
											                 || c.StartsWith( "Permit", StringComparison.OrdinalIgnoreCase );
										          }

										          var Fc1 = IsFixedCharge( c1 );
										          var Fc2 = IsFixedCharge( c2 );

										          return Fc1 switch
										                 {
											                 true when Fc2 => string.CompareOrdinal( c1, c2 ),
											                 true          => 1,
											                 _ => Fc2 switch
											                      {
												                      true => -1,
												                      _    => string.CompareOrdinal( c1, c2 )
											                      }
										                 };
									          }

									          switch( A0 )
									          {
									          case HEADER_COL.CHARGE_LINE:
										          RetVal = Compare( a[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ], b[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] ); // Group Same Taxes and Charges together
										          break;

									          case HEADER_COL.TRIP:
										          RetVal = string.CompareOrdinal( a[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ], b[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ] );

										          if( RetVal == 0 )
											          RetVal = string.CompareOrdinal( a[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ], b[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ] );
										          break;
									          }
								          }
							          }
						          }
					          }
					          return RetVal;
				          } );

				Debug.Write( "Phase 6 -- After Internal Sort", csv );

				string LastColId     = "",
				       LastAccountId = "",
				       LastTripId    = "";

				Row? SaveCol = null;

				// SummariseCsv Trips & Charges
				for( var CurrentRecordIndex = csv.RowCount - 1; CurrentRecordIndex >= 0; CurrentRecordIndex-- )
				{
					var Col       = csv[ CurrentRecordIndex ];
					var ColId     = Col[ 0 ];
					var AccountId = Col[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ];
					var TripId    = Col[ (int)TRIP_LINE.FIELDS.TRIP_ID ];

					if( ( LastColId == ColId ) && ( LastAccountId == AccountId ) && ( LastTripId == TripId ) )
					{
						switch( ColId )
						{
						case HEADER_COL.CHARGE_LINE:
						case HEADER_COL.TRIP:
							SaveCol ??= csv[ CurrentRecordIndex + 1 ];

							if( ColId == HEADER_COL.CHARGE_LINE )
							{
								if( SaveCol[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] == Col[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] ) // Same Charge
								{
									var Val = double.Parse( SaveCol[ (int)CHARGE_LINE.FIELDS.VALUE ] ) + double.Parse( Col[ (int)CHARGE_LINE.FIELDS.VALUE ] );
									SaveCol[ (int)CHARGE_LINE.FIELDS.VALUE ] = Val.ToString( "0.##" );
									csv.RemoveAt( CurrentRecordIndex );
									continue;
								}
								SaveCol = null;
							}
							else
							{
								if( ( SaveCol[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ] == Col[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ] )
								    && ( SaveCol[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ] == Col[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ] ) ) // Same Charge
								{
									var SaveColQty = SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ].AsString.Trim();
									var ColQty     = Col[ (int)PACKAGE_LINE.FIELDS.PIECES ].AsString.Trim();

									double Val;

									if( ( SaveColQty != "" ) && ( ColQty != "" ) )
									{
										Val                                        = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.PIECES ] );
										SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ] = Val.ToString( "0.##" );
									}
									else
										SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ] = "";

									var SaveColWeight = SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ].AsString.Trim();
									var ColWeight     = Col[ (int)PACKAGE_LINE.FIELDS.WEIGHT ].AsString.Trim();

									if( ( SaveColWeight != "" ) && ( ColWeight != "" ) )
									{
										Val                                        = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] );
										SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] = Val.ToString( "0.##" );
									}
									else
										SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] = "";

									Val                                           = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.EXTENSION ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.EXTENSION ] );
									SaveCol[ (int)PACKAGE_LINE.FIELDS.EXTENSION ] = Val.ToString( "0.##" );

									Val                                             = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ] );
									SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ] = Val.ToString( "0.##" );

									Val                                             = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ] );
									SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ] = Val.ToString( "0.##" );

									csv.RemoveAt( CurrentRecordIndex );
									continue;
								}
								SaveCol = null;
							}
							break;

						default:
							SaveCol = null;
							break;
						}
					}
					else
						SaveCol = null;

					LastColId     = ColId;
					LastAccountId = AccountId;
					LastTripId    = TripId;
				}

				Debug.Write( "Phase 7 -- After Summarise", csv );
				Ok = true;
			}
			catch( Exception E )
			{
				Error.Write( E );
			}
			return ( Ok, csv );
		}
	}
}