﻿using System;
using System.IO;
using System.Text;
using Sage300.Globals;
using Utils;
using Utils.Csv;
using File = Utils.File;

namespace Sage300.Csv;

public class BuildExport
{
	public static (bool Ok, int LastInvoiceNumber) BuildExportFiles( string exportPath, Utils.Csv.Csv importCsv,
	                                                                 string salesLedger, string fuelLedger, string ferryLedger, string permitLedger,
	                                                                 string pilotLedger, string storageLedger, string forkliftLedger, string creditCardLedger,
	                                                                 int lastInvoiceNumber )
	{
		var LastInvoiceNumber = lastInvoiceNumber;

		var InvoiceCsv = new Utils.Csv.Csv();
		var InvoiceRow = 0;

		var InvoiceDetailsCsv = new Utils.Csv.Csv();
		var InvoiceDetailsRow = 0;

		var Count = importCsv.RowCount;

		var Invoice = InvoiceCsv[ InvoiceRow ];
		var Details = InvoiceDetailsCsv[ InvoiceDetailsRow ];

		decimal TotalTax1  = 0,
		        TotalTax2  = 0,
		        TotalValue = 0;

		decimal DriverValue = 0;

		var TaxType1 = "";
		var TaxType2 = "";

		var AccountId     = "";
		var InvoiceNumber = "";

		Row DriverLine = null!,
		    CurrentImportLine;

		var NewDriver = true;

		void DoDriver()
		{
			if( !NewDriver )
				DriverLine[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.PRICE ] = DriverValue;

			NewDriver = false;

			DetailLineHeader( SUMMARISE.DETAILS_LINE_TYPE.CHARGES );

			DriverLine = Details;
			Details    = InvoiceDetailsCsv[ ++InvoiceDetailsRow ];

			DriverValue = 0;

			DriverLine[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.ITEM_NUMBER ] = CurrentImportLine[ (int)TRIP_LINE.FIELDS.DRIVER_NAME ].AsString.Trim(); // Diver Code
			DriverLine[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.GL_ACCOUNT ]  = salesLedger;
		}

		void DetailLineHeader( string lineType )
		{
			var D = Details;
			D[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.LINE_TYPE ]      = lineType;
			D[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.ACCOUNT_ID ]     = AccountId;
			D[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.INVOICE_NUMBER ] = InvoiceNumber;
		}

		void DetailChargeLine( string itemCode, decimal value, string ledger, string taxType = "E" )
		{
			DetailLineHeader( SUMMARISE.DETAILS_LINE_TYPE.CHARGES );

			Details[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.ITEM_NUMBER ] = itemCode;
			Details[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.PRICE ]       = value;
			Details[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.TAX_TYPE ]    = taxType;
			Details[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.GL_ACCOUNT ]  = ledger;

			Details = InvoiceDetailsCsv[ ++InvoiceDetailsRow ];
		}

		var Errors = new ErrorList();

		for( var CurrentImportIndex = 0; CurrentImportIndex < Count; ++CurrentImportIndex )
		{
			try
			{
				CurrentImportLine = importCsv[ CurrentImportIndex ];

				switch( CurrentImportLine[ 0 ].AsString.Trim() )
				{
				case HEADER_COL.ACCOUNT:
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.UNUSED ] = "";
					AccountId                                       = CurrentImportLine[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ].AsString.Trim();
					InvoiceNumber                                   = CurrentImportLine[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ].AsString.Trim();
					DoDriver();

					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.COMPANY_NAME ] = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_COMPANY ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.SUITE ]        = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_ADDRESS_1 ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.ADDRESS ]      = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_ADDRESS_2 ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.CITY ]         = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_CITY ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.PROVINCE ]     = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_REGION ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.POST_CODE ]    = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_POST_CODE ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.COUNTRY ]      = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_COUNTRY ].AsString.Trim();
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.PHONE ]        = CurrentImportLine[ (int)TRIP_LINE.FIELDS.BILLING_TELEPHONE ].AsString.Trim();

					var InvoiceDate = CurrentImportLine[ (int)TRIP_LINE.FIELDS.INVOICE_DATE ].AsString.Trim();

					if( !DateTime.TryParse( InvoiceDate, out var Date ) )
						Date = DateTime.Now;

					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.INVOICE_DATE ] = $"{Date:dd/MM/yyyy hh:mm:ss}";

					void SetUpTripWaybillLine( int col, int valueCol )
					{
						DetailLineHeader( SUMMARISE.DETAILS_LINE_TYPE.TRIP );
						Details[ col ] = CurrentImportLine[ valueCol ].AsString.Trim();
						Details        = InvoiceDetailsCsv[ ++InvoiceDetailsRow ];
					}

					// Add the Trip Line
					SetUpTripWaybillLine( (int)SUMMARISE.INVOICE_DETAILS_FIELDS.TRIP_ID, (int)TRIP_LINE.FIELDS.TRIP_ID );

					var WayBill = CurrentImportLine[ (int)TRIP_LINE.FIELDS.WAYBILL ].AsString.Trim();

					if( WayBill.IsNotNullOrWhiteSpace() )
						SetUpTripWaybillLine( (int)SUMMARISE.INVOICE_DETAILS_FIELDS.WAYBILL, (int)TRIP_LINE.FIELDS.WAYBILL );

					break;

				case HEADER_COL.TRIP:
					var Value = CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.EXTENSION ].AsDecimal;
					DriverValue =  Value;
					TotalValue  += Value;

					TaxType1 = CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.TAX_ID_1 ].AsString.Trim();
					var Tax1 = CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ].AsDecimal;
					TotalTax1 += Tax1;

					TaxType2 = CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.TAX_ID_2 ].AsString.Trim();
					var Tax2 = CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ].AsDecimal;
					TotalTax2 += Tax2;

					string  DTaxId;
					decimal DTaxValue;

					if( TaxType2.IsNotNullOrWhiteSpace() )
					{
						DTaxId    = TaxType2;
						DTaxValue = Tax2;
					}
					else
					{
						DTaxId    = TaxType1;
						DTaxValue = Tax1;
					}

					DriverLine[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.TAX_TYPE ]   = DTaxId;
					DriverLine[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.TAX_AMOUNT ] = DTaxValue;

					var FuelSurcharge = CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.FUEL_SURCHARGE_VALUE ].AsDecimal;

					if( FuelSurcharge != 0 )
					{
						TotalValue += FuelSurcharge;
						DetailChargeLine( CurrentImportLine[ (int)PACKAGE_LINE.FIELDS.FUEL_SURCHARGE_ID ].AsString.Trim(), FuelSurcharge, fuelLedger );
					}
					break;

				case HEADER_COL.CHARGE_LINE:
					var    ChargeType = CurrentImportLine[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ].AsString.Trim();
					string Ledger;

					var ChargeValue = CurrentImportLine[ (int)CHARGE_LINE.FIELDS.VALUE ].AsDecimal;
					TotalValue += ChargeValue;

					if( ChargeType.StartsWith( "Ferry", StringComparison.OrdinalIgnoreCase )
					    || ChargeType.StartsWith( "Ferries", StringComparison.OrdinalIgnoreCase ) )
						Ledger = ferryLedger;

					else if( ChargeType.StartsWith( "Permit", StringComparison.OrdinalIgnoreCase ) )
						Ledger = permitLedger;

					else if( ChargeType.StartsWith( "Pilot Car", StringComparison.OrdinalIgnoreCase )
					         || ChargeType.StartsWith( "PilotCar", StringComparison.OrdinalIgnoreCase ) )
						Ledger = pilotLedger;

					else if( ChargeType.StartsWith( "Storage", StringComparison.OrdinalIgnoreCase ) )
						Ledger = storageLedger;

					else if( ChargeType.StartsWith( "Forklift", StringComparison.OrdinalIgnoreCase ) )
						Ledger = forkliftLedger;

					else if( ChargeType.StartsWith( "CcFee", StringComparison.OrdinalIgnoreCase ) )
						Ledger = creditCardLedger;
					else
						Ledger = "";

					if( Ledger.IsNotNullOrWhiteSpace() )
						DetailChargeLine( ChargeType, ChargeValue, Ledger );
					else
					{
						var Price = CurrentImportLine[ (int)CHARGE_LINE.FIELDS.VALUE ].AsDecimal;
						Details[ (int)SUMMARISE.INVOICE_DETAILS_FIELDS.PRICE ] += Price;
						DriverValue                                            += Price;
					}
					break;

				case HEADER_COL.END_OF_INVOICE:
					DoDriver();
					InvoiceDetailsCsv.RemoveAt( InvoiceDetailsRow-- );
					Details = InvoiceDetailsCsv[ InvoiceDetailsRow ];

					NewDriver = true;

					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.ACCOUNT_ID ]     = AccountId;
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.INVOICE_NUMBER ] = InvoiceNumber;

					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.TAX_TYPE_1 ]  = TaxType1;
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.TAX_VALUE_1 ] = TotalTax1;

					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.TAX_TYPE_2 ]  = TaxType2;
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.TAX_VALUE_2 ] = TotalTax2;

					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.TOTAL_EX_TAX ] = TotalValue;
					Invoice[ (int)SUMMARISE.INVOICE_FIELDS.TOTAL ]        = TotalValue + TotalTax1 + TotalTax2;

					TotalTax1  = 0;
					TotalTax2  = 0;
					TotalValue = 0;
					TaxType1   = TaxType2 = "";

					Invoice = InvoiceCsv[ ++InvoiceRow ];

					if( !int.TryParse( InvoiceNumber, out var Num ) )
						Num = 0;

					if( Num > LastInvoiceNumber )
						LastInvoiceNumber = Num;

					break;
				}
			}
			catch( Exception E )
			{
				Errors.Add( $"Error On Invoice {InvoiceNumber}: ( {E.Message} )", CurrentImportIndex + 1 );
			}
		}

		var Now = DateTime.Now;

		void WriteCsv( string prefix, Utils.Csv.Csv wCsv )
		{
			var FileName = Path.Combine( exportPath, File.MakeValidFileName( $"{prefix}_{Now:s}.Csv" ) );

			Utils.Csv.Csv.WriteToFile( wCsv, FileName, Encoding.ASCII );
		}

		WriteCsv( "Invoice File", InvoiceCsv );
		Count = InvoiceDetailsCsv.RowCount;

		if( Count > 0 )
			InvoiceDetailsCsv.RemoveAt( Count - 1 );

		WriteCsv( "Invoice Detail File", InvoiceDetailsCsv );

		return ( Errors.Ok, LastInvoiceNumber );
	}
}