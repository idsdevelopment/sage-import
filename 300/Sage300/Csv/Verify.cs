﻿using System;
using Sage300.Globals;
using Utils.Csv;

namespace Sage300.Csv
{
	public class Verify
	{
		private static int MaxEnum( Type enumType ) => Enum.GetValues( enumType ).GetUpperBound( 0 );

		private static string FileNumberToString( int fieldNumber )
		{
			var Num = fieldNumber.ToString();
			fieldNumber--;

			var CsvCol = "";

			if( fieldNumber > 26 )
			{
				CsvCol      =  "A";
				fieldNumber %= 26;
			}

			CsvCol += ( (char)( 'A' + fieldNumber ) ).ToString();

			return ":" + Num + "  (" + CsvCol + ").";
		}


		public static (bool Ok, Utils.Csv.Csv Csv) VerifyCsv( string remappedCsv )
		{
			var Csv = new Utils.Csv.Csv( remappedCsv );
			Row Cols;

			var Errors = new ErrorList();
			var Rc     = Csv.RowCount;

			int CurrentRecordIndex;

			void IsValidDecimal( int fieldNumber )
			{
				if( !decimal.TryParse( Cols[ fieldNumber ].AsString.Trim(), out var _ ) )
					Errors.Add( "Invalid numeric field" + FileNumberToString( fieldNumber ), CurrentRecordIndex + 1 );
			}

			void IsNotBlank( int fieldNumber )
			{
				if( string.IsNullOrEmpty( Cols[ fieldNumber ].AsString.Trim() ) )
					Errors.Add( "Field cannot be blank" + FileNumberToString( fieldNumber ), CurrentRecordIndex + 1 );
			}

			void IsValidFieldCount( int fieldCount )
			{
				if( Cols.Count < fieldCount )
					Errors.Add( "Invalid field count", CurrentRecordIndex + 1 );
			}

			// Pre Parse for Errors
			for( CurrentRecordIndex = 0; CurrentRecordIndex < Rc; ++CurrentRecordIndex )
			{
				Cols = Csv[ CurrentRecordIndex ];

				if( Cols.Count > 0 )
				{
					switch( Cols[ 0 ].AsString.Trim() )
					{
					case HEADER_COL.ACCOUNT:
						IsValidFieldCount( MaxEnum( typeof( TRIP_LINE.FIELDS ) ) );
						IsNotBlank( (int)TRIP_LINE.FIELDS.ACCOUNT_ID ); // Must be in sage 
						IsNotBlank( (int)TRIP_LINE.FIELDS.INVOICE_NUMBER );
						IsNotBlank( (int)TRIP_LINE.FIELDS.TRIP_ID );

						IsNotBlank( (int)TRIP_LINE.FIELDS.INVOICE_DATE );
						IsNotBlank( (int)TRIP_LINE.FIELDS.CALL_TIME );
						IsNotBlank( (int)TRIP_LINE.FIELDS.DELIVERY_DATE_TIME );
						break;

					case HEADER_COL.TRIP:
						IsValidFieldCount( MaxEnum( typeof( PACKAGE_LINE.FIELDS ) ) );
						IsNotBlank( (int)PACKAGE_LINE.FIELDS.ACCOUNT_ID ); // Must be in sage 
						IsNotBlank( (int)PACKAGE_LINE.FIELDS.INVOICE_NUMBER );
						IsNotBlank( (int)PACKAGE_LINE.FIELDS.TRIP_ID );
						IsNotBlank( (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL );
						IsNotBlank( (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE );
						IsValidDecimal( (int)PACKAGE_LINE.FIELDS.PRICE );
						IsValidDecimal( (int)PACKAGE_LINE.FIELDS.EXTENSION );
						IsValidDecimal( (int)PACKAGE_LINE.FIELDS.FUEL_SURCHARGE_VALUE );
						IsValidDecimal( (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 );
						IsValidDecimal( (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 );
						break;

					case HEADER_COL.CHARGE_LINE:
						IsValidFieldCount( MaxEnum( typeof( CHARGE_LINE.FIELDS ) ) );
						IsNotBlank( (int)CHARGE_LINE.FIELDS.ACCOUNT_ID ); // Must be in sage 
						IsNotBlank( (int)CHARGE_LINE.FIELDS.INVOICE_NUMBER );
						IsNotBlank( (int)CHARGE_LINE.FIELDS.TRIP_ID );
						IsNotBlank( (int)CHARGE_LINE.FIELDS.DESCRIPTION );
						IsValidDecimal( (int)CHARGE_LINE.FIELDS.VALUE );
						break;

					case HEADER_COL.END_OF_INVOICE:
						Cols[ 3 ] = "~~~~~~~~~~~~~~~~~"; //Trip Id
						break;

					default:
						Errors.Add( "Unknown line type.", CurrentRecordIndex + 1 );
						break;
					}
				}
			}
			Debug.Write( "Phase 5 -- After Verify", Csv );
			return ( Errors.Ok, Csv );
		}
	}
}