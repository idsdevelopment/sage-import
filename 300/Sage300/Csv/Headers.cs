﻿namespace Sage300.Csv
{
	internal class SUMMARISE
	{
		internal enum INVOICE_FIELDS
		{
			UNUSED,
			ACCOUNT_ID,
			INVOICE_NUMBER,
			INVOICE_DATE,
			COMPANY_NAME,
			SUITE,
			ADDRESS,
			CITY,
			PROVINCE,
			COUNTRY,
			POST_CODE,
			PHONE,
			TOTAL_EX_TAX,
			TAX_TYPE_2,
			TAX_VALUE_2,
			TAX_TYPE_1,
			TAX_VALUE_1,
			TOTAL
		}

		internal enum INVOICE_DETAILS_FIELDS
		{
			LINE_TYPE,      // A
			ACCOUNT_ID,     // B
			INVOICE_NUMBER, // C
			TRIP_ID,        // D
			ITEM_NUMBER,    // E
			PRICE,          // F
			TAX_TYPE,       // G
			TAX_AMOUNT,     // H
			GL_ACCOUNT,     // I
			WAYBILL         // J
		}

		internal class DETAILS_LINE_TYPE
		{
			public const string TRIP    = "1",
			                    CHARGES = "3";
		}
	}


	internal class HEADER_COL
	{
		internal const string ACCOUNT        = "1",
		                      TRIP           = "2",
		                      CHARGE_LINE    = "3", // Charges must be greater than trips for sort
		                      END_OF_INVOICE = "4";
	}

	internal class PACKAGE_LINE
	{
		internal enum FIELDS
		{
			ACCOUNT_ID = 1,				// B
			INVOICE_NUMBER,				// C
			TRIP_ID,					// D
										 
			SERVICE_LEVEL,				// E
			PACKAGE_TYPE,				// F
			PIECES,						// G
			WEIGHT,						// H
			PRICE,						// I
			EXTENSION,					// J
										 
			FUEL_SURCHARGE_ID,			// K
			FUEL_SURCHARGE_VALUE,		// L
										 
			TAX_ID_1,					// M
			TAX_VALUE_1,				// N
			TAX_ID_2,					// O
			TAX_VALUE_2					// P
		}
	}

	internal class CHARGE_LINE
	{
		internal enum FIELDS
		{
			ACCOUNT_ID = 1,
			INVOICE_NUMBER,
			TRIP_ID,

			DESCRIPTION,
			VALUE
		}
	}


	internal class TRIP_LINE
	{
		internal enum FIELDS
		{
			ACCOUNT_ID = 1, // B
			INVOICE_NUMBER, // C
			TRIP_ID,        // D

			INVOICE_DATE, // E

			CALL_TIME,          // F
			DELIVERY_DATE_TIME, // G

			PICKUP_TIME,   // H
			DELIVERY_TIME, // I

			PICKUP_REFERENCE, // J
			PICKUP_COMPANY,   // K
			PICKUP_ADDRESS_1, // L
			PICKUP_ADDRESS_2, // M
			PICKUP_CITY,      // N
			PICKUP_REGION,    // O
			PICKUP_COUNTRY,   // P
			PICKUP_POST_CODE, // Q
			PICKUP_ZONE,      // R
			PICKUP_CONTACT,   // S
			PICKUP_TELEPHONE, // T
			PICKUP_EMAIL,     // U
			PICKUP_NOTES,     // V

			DELIVERY_REFERENCE, // W
			DELIVERY_COMPANY,   // X
			DELIVERY_ADDRESS_1, // Y
			DELIVERY_ADDRESS_2, // Z
			DELIVERY_CITY,      // AA
			DELIVERY_REGION,    // AB
			DELIVERY_POST_CODE, // AC
			DELIVERY_ZONE,      // AD
			DELIVERY_COUNTRY,   // AE
			DELIVERY_CONTACT,   // AF
			DELIVERY_TELEPHONE, // AG
			DELIVERY_EMAIL,     // AH
			DELIVERY_NOTES,     // AI

			BILLING_COMPANY,   // AJ
			BILLING_ADDRESS_1, // AK
			BILLING_ADDRESS_2, // AL
			BILLING_CITY,      // AM
			BILLING_REGION,    // AN
			BILLING_POST_CODE, // AO
			BILLING_COUNTRY,   // AP
			BILLING_CONTACT,   // AQ
			BILLING_TELEPHONE, // AR
			BILLING_EMAIL,     // AS

			SHIPPING_COMPANY,   // AT
			SHIPPING_ADDRESS_1, // AU
			SHIPPING_ADDRESS_2, // AV
			SHIPPING_CITY,      // AW
			SHIPPING_REGION,    // AX
			SHIPPING_POST_CODE, // AY
			SHIPPING_COUNTRY,   // AZ
			SHIPPING_CONTACT,   // BA
			SHIPPING_TELEPHONE, // BB
			SHIPPING_EMAIL,     // BC

			DRIVER_NAME,   // BD
			CALL_TAKER_ID, // BE

			// BF
			WAYBILL // BG
		}
	}
}