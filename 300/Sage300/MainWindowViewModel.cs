﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using AzureRemoteService;
using Protocol.Data;
using Sage300.Communications;
using Sage300.Csv;
using Sage300.Globals;
using Utils;
using Utils.Csv;
using XamlViewModel.Wpf;

namespace Sage300;

public class MainWindowViewModel : ViewModelBase
{
	public enum SERVER
	{
		UNKNOWN,
		WEST,
		EAST,
		CORE
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();
		WhenServerChanges();
	}

#region Diagnostics
	[Setting]
	public string ErrorPath
	{
		get { return Get( () => ErrorPath, @"C:\Ids\Error" ); }
		set { Set( () => ErrorPath, value ); }
	}

	[Setting]
	public bool DebugMode
	{
		get { return Get( () => DebugMode, false ); }
		set { Set( () => DebugMode, value ); }
	}

	[Setting]
	public string DebugPath
	{
		get { return Get( () => DebugPath, @"C:\Ids\Debug" ); }
		set { Set( () => DebugPath, value ); }
	}

	[Setting]
	public bool DontUpdateLastInvoiceNumber
	{
		get { return Get( () => DontUpdateLastInvoiceNumber, false ); }
		set { Set( () => DontUpdateLastInvoiceNumber, value ); }
	}
#endregion

#region Window
	[Setting]
	public int Top
	{
		get { return Get( () => Top, 0 ); }
		set { Set( () => Top, value ); }
	}

	[Setting]
	public int Left
	{
		get { return Get( () => Left, 0 ); }
		set { Set( () => Left, value ); }
	}

	[Setting]
	public int Height
	{
		get { return Get( () => Height, 500 ); }
		set { Set( () => Height, value ); }
	}

	[Setting]
	public int Width
	{
		get { return Get( () => Width, 830 ); }
		set { Set( () => Width, value ); }
	}

	public bool ToolbarIconVisible
	{
		get { return Get( () => ToolbarIconVisible, false ); }
		set { Set( () => ToolbarIconVisible, value ); }
	}

	public bool Enabled
	{
		get { return Get( () => Enabled, true ); }
		set { Set( () => Enabled, value ); }
	}

	public bool Running
	{
		get { return Get( () => Running, false ); }
		set { Set( () => Running, value ); }
	}
#endregion

#region Export
	[Setting]
	public string ExportPath
	{
		get { return Get( () => ExportPath, "" ); }
		set { Set( () => ExportPath, value ); }
	}

	[Setting]
	public string SalesLedger
	{
		get { return Get( () => SalesLedger, "" ); }
		set { Set( () => SalesLedger, value ); }
	}

	[Setting]
	public string FuelLedger
	{
		get { return Get( () => FuelLedger, "" ); }
		set { Set( () => FuelLedger, value ); }
	}

	[Setting]
	public string FerryLedger
	{
		get { return Get( () => FerryLedger, "" ); }
		set { Set( () => FerryLedger, value ); }
	}

	[Setting]
	public string PermitLedger
	{
		get { return Get( () => PermitLedger, "" ); }
		set { Set( () => PermitLedger, value ); }
	}

	[Setting]
	public string PilotCarLedger
	{
		get { return Get( () => PilotCarLedger, "" ); }
		set { Set( () => PilotCarLedger, value ); }
	}

	[Setting]
	public string StorageLedger
	{
		get { return Get( () => StorageLedger, "" ); }
		set { Set( () => StorageLedger, value ); }
	}

	[Setting]
	public string ForkliftLedger
	{
		get { return Get( () => ForkliftLedger, "" ); }
		set { Set( () => ForkliftLedger, value ); }
	}


	[Setting]
	public string CreditCardLedger
	{
		get { return Get( () => CreditCardLedger, "" ); }
		set { Set( () => CreditCardLedger, value ); }
	}


	[Setting]
	public int LastInvoiceNumber
	{
		get { return Get( () => LastInvoiceNumber, 0 ); }
		set { Set( () => LastInvoiceNumber, value ); }
	}

	public int EndingInvoiceNumber
	{
		get { return Get( () => EndingInvoiceNumber, 999999999 ); }
		set { Set( () => EndingInvoiceNumber, value ); }
	}

	[Setting]
	public int InventoryType
	{
		get { return Get( () => InventoryType, 0 ); }
		set { Set( () => InventoryType, value ); }
	}
#endregion

#region IDS Settings
	[Setting]
	public SERVER WestServer
	{
		get { return Get( () => WestServer, SERVER.WEST ); }
		set { Set( () => WestServer, value ); }
	}

	[Setting]
	public SERVER EastServer
	{
		get { return Get( () => EastServer, SERVER.UNKNOWN ); }
		set { Set( () => EastServer, value ); }
	}

	[Setting]
	public SERVER CoreServer
	{
		get { return Get( () => CoreServer, SERVER.UNKNOWN ); }
		set { Set( () => CoreServer, value ); }
	}

	[DependsUpon( nameof( WestServer ) )]
	[DependsUpon( nameof( EastServer ) )]
	[DependsUpon( nameof( CoreServer ) )]
	public void WhenServerChanges()
	{
		NeedAccountId = CoreServer != SERVER.CORE;
	}

	[Setting]
	public string CarrierId
	{
		get { return Get( () => CarrierId, "" ); }
		set { Set( () => CarrierId, value ); }
	}

	[Setting]
	public string AccountId
	{
		get { return Get( () => AccountId, "" ); }
		set { Set( () => AccountId, value ); }
	}

	public bool NeedAccountId
	{
		get { return Get( () => NeedAccountId, true ); }
		set { Set( () => NeedAccountId, value ); }
	}

	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}

	[Setting]
	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}

	public bool SettingsOk
	{
		get { return Get( () => SettingsOk, false ); }
		set { Set( () => SettingsOk, value ); }
	}

	[DependsUpon150( nameof( ExportPath ) )]
	[DependsUpon150( nameof( SalesLedger ) )]
	[DependsUpon150( nameof( FuelLedger ) )]
	[DependsUpon150( nameof( FerryLedger ) )]
	[DependsUpon150( nameof( PermitLedger ) )]
	[DependsUpon150( nameof( CarrierId ) )]
	[DependsUpon150( nameof( AccountId ) )]
	[DependsUpon150( nameof( UserName ) )]
	[DependsUpon150( nameof( Password ) )]
	[DependsUpon150( nameof( ErrorPath ) )]
	[DependsUpon150( nameof( NextExportDate ) )]
	[DependsUpon150( nameof( NextExportTime ) )]
	[DependsUpon150( nameof( LastInvoiceNumber ) )]
	[DependsUpon150( nameof( EndingInvoiceNumber ) )]
	[DependsUpon150( nameof( DebugMode ) )]
	[DependsUpon150( nameof( DebugPath ) )]
	public bool EnableOk()
	{
		var Enab = ExportPath.IsNotNullOrWhiteSpace() && SalesLedger.IsNotNullOrWhiteSpace() && FuelLedger.IsNotNullOrWhiteSpace()
		           && FerryLedger.IsNotNullOrWhiteSpace() && PermitLedger.IsNotNullOrWhiteSpace()
		           && CarrierId.IsNotNullOrWhiteSpace() && UserName.IsNotNullOrWhiteSpace() && Password.IsNotNullOrWhiteSpace()
		           && ErrorPath.IsNotNullOrWhiteSpace();

		Enab = Enab && ( !NeedAccountId || ( NeedAccountId && AccountId.IsNotNullOrWhiteSpace() ) );

		if( Enab && ( LastInvoiceNumber <= EndingInvoiceNumber ) )
			return SettingsOk = !DebugMode || DebugPath.IsNotNullOrWhiteSpace();

		return SettingsOk = false;
	}
#endregion

#region Commands
	public Action? OnShowWindow;

	public ICommand ShowWindow => Commands[ nameof( ShowWindow ) ];

	public void Execute_ShowWindow()
	{
		OnShowWindow?.Invoke();
	}

	public ICommand ManualImport => Commands[ nameof( ManualImport ) ];

	public void Execute_ManualImport()
	{
		Tasks.RunVoid( Import );
	}

	public async void Import()
	{
		if( !Running )
		{
			Enabled = false;
			Running = true;

			try
			{
				var Ep = Error.ErrorPath = ErrorPath;
				Directory.CreateDirectory( Ep );

				Debug.DebugMode = DebugMode;
				var DbPath = Debug.DebugPath = DebugPath;
				Directory.CreateDirectory( DbPath );

				var ExPath = ExportPath;
				Directory.CreateDirectory( ExPath );

				string CsvText;

				var IsCore = CoreServer == SERVER.CORE;

				if( IsCore )
				{
					try
					{
						if( DebugMode )
							Azure.Slot = Azure.SLOT.ALPHA_TRW;

						var (Status, Client) = await Azure.LogIn( "Sage 300", CarrierId, UserName, Password );

						if( Status == Azure.AZURE_CONNECTION_STATUS.OK )
						{
							CsvText = await Client.RequestExportCsv( new ExportArgs
							                                         {
								                                         ExportName = "Sage300",
								                                         StringArg1 = LastInvoiceNumber.ToString() // Starting Invoice Number
							                                         } );

							Debug.Write( "Pre-Phase 1 -- Data Imported From Core", CsvText );

							var HeaderEnd = CsvText.IndexOf( "\r\n", StringComparison.Ordinal );

							// Get the header line
							var Header = CsvText[ ..HeaderEnd ];

							string StripQuotes( string s )
							{
								if( s is ['"', _, ..] && ( s[ ^1 ] == '"' ) )
									s = s[ 1..^1 ];

								return s.Trim();
							}

							// Remove the header line
							CsvText = CsvText[ ( HeaderEnd + 2 ).. ];

							var Charges = Header.Split( ',' )
							                    .Select( StripQuotes )
							                    .ToArray();

							var Csv = new Utils.Csv.Csv( CsvText );

							foreach( Row Row in Csv )
							{
								for( int I = (int)ReMap.FIELDS.IDS_BEGIN_CHARGES, Limit = Charges.Length;
								     I < Limit;
								     I++ )
								{
									var Cell = Row[ I ];
									var Val  = StripQuotes( Cell.AsString );
									Cell.AsString = $"CHG_{Charges[ I ]}::{Val}";
								}
							}

							CsvText = Csv.ToString();
						}
						else
							throw new Exception();
					}
					catch
					{
						Error.Write( "Cannot connect to the Core server." );
						return;
					}
				}
				else
				{
					using var Client = new Client();

					var (Ok, Value) = Client.GetCsv( WestServer == SERVER.WEST ? WestServer : EastServer, CarrierId, AccountId, UserName, Password, LastInvoiceNumber ).Result;

					if( Ok )
						CsvText = Value.Trim();
					else
					{
						Error.Write( Value );
						return;
					}
				}

				Debug.Write( "Phase 1 -- Data Imported From Ids-Core", CsvText );

				if( CsvText.IsNotNullOrWhiteSpace() )
				{
					var ReMapped = ReMap.ReMapCsv( CsvText, DebugMode, DebugPath );
					Debug.Write( "Phase 2 -- After re-map Csv", ReMapped );

					var (VerifyOk, Csv) = Verify.VerifyCsv( ReMapped );

					if( VerifyOk )
					{
						( VerifyOk, Csv ) = Summarise.SummariseCsv( Csv );
						Debug.Write( "Phase 3 -- After summarise", Csv );

						if( VerifyOk )
						{
							var (Ok, LastInv) = BuildExport.BuildExportFiles( ExPath, Csv,
							                                                  SalesLedger, FuelLedger, FerryLedger, PermitLedger,
							                                                  PilotCarLedger, StorageLedger, ForkliftLedger, CreditCardLedger,
							                                                  LastInvoiceNumber );

							if( Ok )
							{
								if( !DontUpdateLastInvoiceNumber )
									LastInvoiceNumber = LastInv;
								SaveSettings();
							}
						}
					}
				}
			}
			finally
			{
				Enabled = true;
				Running = false;
			}
		}
	}
#endregion

#region Schedule
	[Setting]
	public DateTime NextExportDate
	{
		get { return Get( () => NextExportDate, DateTime.Now.AddHours( 24 ) ); }
		set { Set( () => NextExportDate, value ); }
	}

	[Setting]
	public DateTime NextExportTime
	{
		get { return Get( () => NextExportTime, DateTime.Now.AddHours( 24 ) ); }
		set { Set( () => NextExportTime, value ); }
	}

	[Setting]
	public int Hours
	{
		get { return Get( () => Hours, 24 ); }
		set { Set( () => Hours, value ); }
	}

	// ReSharper disable once NotAccessedField.Local
	private Timer?   ImportTimer;
	private DateTime NextImportTime;
	private bool     ImportRunning;

	[DependsUpon250( nameof( NextExportDate ) )]
	public TimeSpan CalculateNextImportTime()
	{
		var ExportTimeOfDay = NextExportTime.TimeOfDay;

		NextImportTime = NextExportDate.Date + ExportTimeOfDay;

		return ExportTimeOfDay;
	}

	private DateTime UpdateNextImportTime( DateTime now, TimeSpan exportTimeOfDay )
	{
		var Next = now.Date.Add( exportTimeOfDay ).AddHours( Hours );
		NextExportDate = Next.Date;
		return DateTime.MinValue.Add( Next.TimeOfDay );
	}

	[DependsUpon250( nameof( Hours ) )]
	public void UpdateNextImportTime()
	{
		UpdateNextImportTime( DateTime.Now, NextExportTime.TimeOfDay );
	}

	[DependsUpon( nameof( ToolbarIconVisible ) )]
	public void StartImportTimer()
	{
		if( ToolbarIconVisible && !ImportRunning )
		{
		#if !DEBUG
				DontUpdateLastInvoiceNumber = false;
		#endif
			var ExportTimeOfDay = CalculateNextImportTime();

			ImportTimer?.Dispose();

			ImportTimer = new Timer( _ =>
			                         {
				                         if( ToolbarIconVisible && !ImportRunning )
				                         {
					                         ImportRunning = true;
					                         var Now = DateTime.Now;

					                         if( Now >= NextImportTime )
					                         {
						                         try
						                         {
							                         Import();
						                         }
						                         catch( Exception E )
						                         {
							                         Console.WriteLine( E );
						                         }
						                         finally
						                         {
							                         NextExportTime = UpdateNextImportTime( Now, ExportTimeOfDay );
							                         SaveSettings();
							                         ImportRunning = false;
						                         }
					                         }
				                         }
			                         }, this, 1000, 1000 );
		}
	}
#endregion
}