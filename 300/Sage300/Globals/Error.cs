﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using File = Utils.File;

namespace Sage300.Globals
{
	internal class ErrorEntry
	{
		internal string Error = null!;
		internal int    LineNumber;
	}

	internal class ErrorList : List<ErrorEntry>
	{
		public bool Ok
		{
			get
			{
				if( Count > 0 )
				{
					Error.Write( ToString() );
					return false;
				}
				return true;
			}
		}

		internal void Add( string error, int lineNumber )
		{
			base.Add( new ErrorEntry { Error = error, LineNumber = lineNumber } );
		}

		public override string ToString()
		{
			var Retval = new StringBuilder();

			foreach( var Entry in this )
				Retval.Append( $"Line Number: {Entry.LineNumber}, Error: {Entry.Error}\r\n" );

			return Retval.ToString();
		}
	}

	public class Error
	{
#pragma warning disable CA2211 // Non-constant fields should not be visible
		public static string? ErrorPath;
#pragma warning restore CA2211 // Non-constant fields should not be visible

		public static void Write( string reason )
		{
			if( ErrorPath is not null )
			{
				var       FileName = File.MakeValidFileName( $"{DateTime.Now:s}.Error.Log" );
				using var Stream   = new FileStream( Path.Combine( ErrorPath, FileName ), FileMode.Create, FileAccess.ReadWrite );
				var       Bytes    = Encoding.UTF8.GetBytes( reason );
				Stream.Write( Bytes );
				Stream.Close();
			}
		}

		public static void Write( Exception e )
		{
			Write( e.Message );
		}
	}
}