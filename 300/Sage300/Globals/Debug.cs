﻿using System.IO;
using System.Text;
using Utils;

namespace Sage300.Globals
{
	public class Debug
	{
		public const string CSV_EXTENSION = ".csv";

		private static bool _DebugMode;

#pragma warning disable CA2211 // Non-constant fields should not be visible
		public static string? DebugPath;
#pragma warning restore CA2211 // Non-constant fields should not be visible

		public static bool DebugMode
		{
			get => _DebugMode && DebugPath.IsNotNullOrWhiteSpace();
			set => _DebugMode = value;
		}

		public static void Write( string fileName, Utils.Csv.Csv csv )
		{
			if( DebugMode )
			{
				try
				{
					Utils.Csv.Csv.WriteToFile( csv, Path.Combine( DebugPath!, $"{fileName}{CSV_EXTENSION}" ) );
				}
				catch
				{
				}
			}
		}

		public static void Write( string fileName, string csvAsText )
		{
			if( DebugMode )
			{
				try
				{
					using var Stream = new FileStream( Path.Combine( DebugPath!, $"{fileName}{CSV_EXTENSION}" ), FileMode.Create, FileAccess.ReadWrite );
					var       Bytes  = Encoding.UTF8.GetBytes( csvAsText );
					Stream.Write( Bytes, 0, Bytes.Length );
					Stream.Close();
				}
				catch
				{
				}
			}
		}
	}
}