﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Media;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Win32;
using Utils;

namespace Sage300;

public class ServerConvertor : IValueConverter
{
	public object Convert( object? value, Type targetType, object? parameter, CultureInfo culture )
	{
		if( value is MainWindowViewModel.SERVER Server && parameter is string Parameter )
		{
			return Server switch
			       {
				       MainWindowViewModel.SERVER.WEST when Parameter.Equals( "WEST", StringComparison.OrdinalIgnoreCase ) => true,
				       MainWindowViewModel.SERVER.EAST when Parameter.Equals( "EAST", StringComparison.OrdinalIgnoreCase ) => true,
				       MainWindowViewModel.SERVER.CORE when Parameter.Equals( "CORE", StringComparison.OrdinalIgnoreCase ) => true,
				       _                                                                                                   => false
			       };
		}
		return false;
	}

	public object ConvertBack( object? value, Type targetType, object? parameter, CultureInfo culture )
	{
		if( value is bool Value && parameter is string Parameter )
		{
			return Value switch
			       {
				       true when Parameter.Equals( "WEST", StringComparison.OrdinalIgnoreCase ) => MainWindowViewModel.SERVER.WEST,
				       true when Parameter.Equals( "EAST", StringComparison.OrdinalIgnoreCase ) => MainWindowViewModel.SERVER.EAST,
				       true when Parameter.Equals( "CORE", StringComparison.OrdinalIgnoreCase ) => MainWindowViewModel.SERVER.CORE,
				       _                                                                        => MainWindowViewModel.SERVER.UNKNOWN
			       };
		}
		return MainWindowViewModel.SERVER.UNKNOWN;
	}
}

/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
	private readonly MainWindowViewModel Model;

	public MainWindow()
	{
		InitializeComponent();
		var M = (MainWindowViewModel)Root.DataContext;
		Model = M;

		M.OnShowWindow = () =>
		                 {
			                 WindowState = WindowState.Normal;
		                 };

		TrayPopup.DataContext = M;
		Password.Password     = M.Password;
		M.EnableOk();
	}

	private void TextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
	{
		var Bad = e.Handled = !e.Text.IsInteger();

		if( Bad )
			SystemSounds.Beep.Play();
	}

	private void Export_Button_Click( object sender, RoutedEventArgs e )
	{
		var Folder = ExportFolder.Text.Trim();

		var Dlg = new OpenFolderDialog
		          {
			          FolderName       = Folder,
			          InitialDirectory = Folder,
			          Title            = "Select CSV Export Folder"
		          };

		if( Dlg.ShowDialog() == true )
			ExportFolder.Text = Dlg.FolderName;
	}

	private void Window_Closing( object sender, CancelEventArgs e )
	{
		var Button = MessageBox.Show( "Close to the Icon Tray?", "Confirm", MessageBoxButton.YesNoCancel, MessageBoxImage.Question );

		switch( Button )
		{
		case MessageBoxResult.Yes:
			e.Cancel                 = true;
			Model.ToolbarIconVisible = true;
			WindowState              = WindowState.Minimized;
			break;

		case MessageBoxResult.No:
			TaskbarIcon.Visibility = Visibility.Collapsed;
			Model.SaveSettings();
			break;
		}
	}

	private void PasswordBox_PasswordChanged( object sender, RoutedEventArgs e )
	{
		Model.Password = Password.Password;
	}

	private void Error_Button_Click( object sender, RoutedEventArgs e )
	{
		var Folder = ErrorFolder.Text.Trim();

		var Dlg = new OpenFolderDialog
		          {
			          FolderName       = Folder,
			          InitialDirectory = Folder,
			          Title            = "Select Error Output Folder"
		          };

		if( Dlg.ShowDialog() == true )
			ErrorFolder.Text = Dlg.FolderName;
	}

	private void Debug_Button_Click( object sender, RoutedEventArgs e )
	{
		var Folder = DebugFolder.Text.Trim();

		var Dlg = new OpenFolderDialog
		          {
			          FolderName       = Folder,
			          InitialDirectory = Folder,
			          Title            = "Select Debug Output Folder"
		          };

		if( Dlg.ShowDialog() == true )
			DebugFolder.Text = Dlg.FolderName;
	}

	private void Root_StateChanged( object sender, EventArgs e )
	{
		var Minimised = WindowState == WindowState.Minimized;

		Model.ToolbarIconVisible = Minimised;
		ShowInTaskbar            = !Minimised;
	}
}